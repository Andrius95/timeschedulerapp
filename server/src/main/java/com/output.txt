Folder PATH listing
Volume serial number is E42C-5184
D:.
|   output.txt
|   
\---timeScheduler
    |   colorGraph.java
    |   DataInit.java
    |   mColoring.java
    |   mColoringProblem.java
    |   TimeSchedulerApplication.java
    |   
    +---Activity
    |       Activity.java
    |       ActivityController.java
    |       ActivityRepository.java
    |       ActivityService.java
    |       ActivityServiceImpl.java
    |       
    +---Address
    |       Address.java
    |       
    +---Classroom
    |       Classroom.java
    |       ClassroomController.java
    |       ClassroomRepository.java
    |       ClassroomService.java
    |       ClassroomServiceImpl.java
    |       
    +---Course
    |       Course.java
    |       CourseRepository.java
    |       
    +---Graph
    |       Graph.java
    |       GraphCalculationService.java
    |       GraphCalculationServiceImpl.java
    |       
    +---Group
    |       Group.java
    |       GroupController.java
    |       GroupRepository.java
    |       
    +---Subject
    |       Subject.java
    |       SubjectController.java
    |       SubjectRepository.java
    |       SubjectService.java
    |       SubjectServiceImpl.java
    |       
    +---Teacher
    |       Teacher.java
    |       TeacherController.java
    |       TeacherRepository.java
    |       
    +---TeacherInfo
    |       TeacherInfo.java
    |       
    +---Times
    |       TimeRepository.java
    |       Times.java
    |       
    \---TimeTable
            TimeTable.java
            TimeTableController.java
            TimeTableRepository.java
            TimeTableService.java
            TimeTableServiceImpl.java
            
