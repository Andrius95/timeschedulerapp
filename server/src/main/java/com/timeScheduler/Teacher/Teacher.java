package com.timeScheduler.Teacher;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.timeScheduler.Subject.Subject;
import com.timeScheduler.TeacherInfo.TeacherInfo;
import com.timeScheduler.Times.Times;

import javax.persistence.*;
import java.util.List;

@Entity
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Teacher_Time",
            joinColumns = { @JoinColumn(name = "teacher_id") },
            inverseJoinColumns = { @JoinColumn(name = "time_id") }
    )
    @JsonBackReference(value="anyName")
    private List<Times> times;

    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacher_id")
    @JsonBackReference(value="anyName2")
    @OrderColumn(name="teacher")
    private List<Subject> subjects;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "teacherInfo_id", referencedColumnName = "id")
    private TeacherInfo teacherInfo;

    public Teacher() {
    }

    public List<Times> getTimes() {
        return times;
    }

    public void setTimes(List<Times> times) {
        this.times = times;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TeacherInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }


}
