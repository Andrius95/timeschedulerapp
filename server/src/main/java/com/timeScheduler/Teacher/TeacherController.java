package com.timeScheduler.Teacher;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RequestMapping("/teacher")
@RestController
public class TeacherController {
    private TeacherRepository repository;

    public TeacherController(TeacherRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/all-teachers")
    @CrossOrigin(origins = "http://localhost:3000")
    public Collection<Teacher> manTeachers() {
        List<Teacher> manTeachers = repository.findAll();

        return manTeachers;
    }

    @DeleteMapping("/delete-teacher/{id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public void deleteTeacher(@PathVariable Long id) {
        repository.deleteById(id);
    }

    @PostMapping(value = "/add-teacher",consumes = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "*")
    public void addTeacher(@RequestBody Teacher teacher) {
        repository.save(teacher);
    }




}
