package com.timeScheduler.Activity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.timeScheduler.Classroom.Classroom;
import com.timeScheduler.Subject.Subject;
import com.timeScheduler.TimeTable.TimeTable;

import javax.persistence.*;

/**
 * Entity class that represents concrete activity, with known classroom, subject and student group.
 */
@Entity
public class Activity implements Comparable<Activity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    //  @JsonBackReference
    @JoinColumn(name = "classroom_id")
    private Classroom classroom;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "timeTable_id")
    @JsonBackReference
    private TimeTable timeTable;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id")
    private Subject subject;

    private int timeSlot;
    private int time;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(int timeSlot) {
        this.timeSlot = timeSlot;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public TimeTable getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(TimeTable timeTable) {
        this.timeTable = timeTable;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public int compareTo(Activity activity) {
        if (getSubject().getCourseName() == null || activity.getSubject().getCourseName() == null) {
            return 0;
        }
        return activity.getSubject().getCourseName().compareTo(getSubject().getCourseName());
    }
}
