package com.timeScheduler.Activity;


import com.timeScheduler.TimeTable.TimeTable;
import com.timeScheduler.TimeTable.TimeTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RequestMapping("/activity")
@RestController
public class ActivityController {

    private ActivityRepository repository;

    private TimeTableRepository timeTableRepository;

    @Autowired
    private ActivityService activityService;

    public ActivityController(ActivityRepository repository, TimeTableRepository timeTableRepository) {
        this.repository = repository;
        this.timeTableRepository = timeTableRepository;
    }

    @GetMapping("/all-activities")
    @CrossOrigin(origins = "http://localhost:3000")
    public Collection<Activity> allActivities() {
        List<TimeTable> timeTableList = timeTableRepository.findAll();
        TimeTable newestTimeTable = timeTableList.stream().max(Comparator.comparing(TimeTable::getCreatedOn)).orElse(null);
        if (newestTimeTable != null) {
            return newestTimeTable.getActivities();
        }
        return new ArrayList<>();
    }

    @GetMapping("/group-activities")
    @CrossOrigin(origins = "http://localhost:3000")
    public Collection<Activity> groupActivities() {
        List<TimeTable> timeTableList = timeTableRepository.findAll();
        TimeTable newestTimeTable = timeTableList.stream().max(Comparator.comparing(TimeTable::getCreatedOn)).orElse(null);
        if (newestTimeTable == null || newestTimeTable.getActivities() == null) {
            return new ArrayList<>();
        }
        List<Activity> allactivities = newestTimeTable.getActivities();
        Collections.sort(allactivities, new ActivityComparator());
        return allactivities;
    }


    @GetMapping("/classroom-activities")
    @CrossOrigin(origins = "http://localhost:3000")
    public Collection<Activity> classroomActivities() {
        List<Activity> allactivities = repository.findAll();
        Collections.sort(allactivities, new ActivityComparator());
        return allactivities;
    }


    @GetMapping("/teacher-activities")
    @CrossOrigin(origins = "http://localhost:3000")
    public Collection<Activity> teacherActivities() {
        List<Activity> allactivities = repository.findAll();

        return allactivities;
    }


    public ActivityService getActivityService() {

        return activityService;
    }

    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }
}


