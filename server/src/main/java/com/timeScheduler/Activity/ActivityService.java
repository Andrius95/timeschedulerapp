package com.timeScheduler.Activity;

import com.timeScheduler.Subject.Subject;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ActivityService {

    /**
     * Initiates activities by subjects.
     * Initiates as many activities as subject is planned to be tought for a week.
     *
     * @param subjectList by which activities are initiated
     * @return list of newly created activities
     */
    List<Activity> initiateActivitiesBySubjects(List<Subject> subjectList);

}
class ActivityComparator implements Comparator<Activity> {
    public int compare(Activity a1, Activity a2) {

        int nameComp = a1.getSubject().getGroups().get(0).getCourse().getName().compareTo(a2.getSubject().getGroups().get(0).getCourse().getName());
        nameComp = (nameComp == 0) ? a1.getSubject().getGroups().get(0).getNumber()-a2.getSubject().getGroups().get(0).getNumber(): nameComp;

        return ((nameComp == 0) ? a1.getTime()-a2.getTime() : nameComp);
    }
}
