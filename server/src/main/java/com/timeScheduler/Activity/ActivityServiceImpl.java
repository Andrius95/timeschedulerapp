package com.timeScheduler.Activity;

import com.timeScheduler.Subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public List<Activity> initiateActivitiesBySubjects(List<Subject> subjectList) {
        List<Activity> result = new ArrayList<>();
        for (Subject subject : subjectList) {
            for (int i = 0; i < subject.getTimesPerWeek(); i++) {
                Activity createdActivity = new Activity();
                createdActivity.setSubject(subject);
                //uzpildyti -1 visus timeslot
                createdActivity.setTimeSlot(-1);
                activityRepository.save(createdActivity);
                result.add(createdActivity);
            }
        }

        return result;

    }





    public ActivityRepository getActivityRepository() {
        return activityRepository;
    }

    public void setActivityRepository(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }
}
