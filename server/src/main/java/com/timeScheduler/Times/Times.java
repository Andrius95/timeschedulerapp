package com.timeScheduler.Times;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.timeScheduler.Teacher.Teacher;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Times {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int time;
    private String weekDay;
    private String hours;

    @ManyToMany(mappedBy = "times")
    private List<Teacher> teachers;

    public Times() {
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }
}

