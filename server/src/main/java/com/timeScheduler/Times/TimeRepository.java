package com.timeScheduler.Times;

import com.timeScheduler.Course.Course;
import com.timeScheduler.TimeTable.TimeTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

//@Repository
@RepositoryRestResource
public interface TimeRepository extends JpaRepository<Times, Long> {

}
