package com.timeScheduler.TimeTable;

import com.timeScheduler.Activity.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/timetable")
@RestController
public class TimeTableController {

    @Autowired
    private TimeTableService timeTableService;

    @GetMapping("/calculate-greedy-timetable")
    @CrossOrigin(origins = "http://localhost:3000")
    public TimeTable calculateGreedyTimeTable() {
        return timeTableService.calculateGreedyTimeTable();
    }

    @GetMapping("/calculate-backtracking-timetable")
    @CrossOrigin(origins = "http://localhost:3000")
    public TimeTable calculateBackTrackingTimeTable() {
        return timeTableService.calculateBackTrackingTimeTable();
    }

    @GetMapping("/calculate-timetable/{timeTableType}")
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Activity> calculateTimeTable(@PathVariable String timeTableType) {
        return timeTableService.calculateChosenTimeTable(timeTableType).getActivities();
    }

    public TimeTableService getTimeTableService() {
        return timeTableService;
    }

    public void setTimeTableService(TimeTableService timeTableService) {
        this.timeTableService = timeTableService;
    }
}
