package com.timeScheduler.TimeTable;

import com.timeScheduler.Activity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface TimeTableRepository extends JpaRepository<TimeTable, Long> {
}
