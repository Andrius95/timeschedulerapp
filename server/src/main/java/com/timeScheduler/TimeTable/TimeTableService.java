package com.timeScheduler.TimeTable;

/**
 * TimeTable creation and calculation service.
 */
public interface TimeTableService {

    TimeTable calculateChosenTimeTable(String timeTableType);

    /**
     * Creates new timetable and invokes calculation service to fill it with activities,
     * and assigns classrooms to activities. This calculation uses greedy algorithm.
     *
     * @return calculated {@link TimeTable}
     */
    TimeTable calculateGreedyTimeTable();

    /**
     * Creates new timetable and invokes calculation service to fill it with activities,
     * and assigns classrooms to activities. This calculation uses backtracking algorithm.
     *
     * @return calculated {@link TimeTable}
     */
    TimeTable calculateBackTrackingTimeTable();
}
