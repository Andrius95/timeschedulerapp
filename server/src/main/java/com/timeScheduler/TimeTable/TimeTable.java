package com.timeScheduler.TimeTable;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.timeScheduler.Activity.Activity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Entity class that represents Timetable.
 */
@Entity
public class TimeTable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String description;

    private Date createdOn;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "timeTable_id")
    @OrderColumn(name="activities")
    @JsonManagedReference
    private List<Activity> activities;

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
}
