package com.timeScheduler.TimeTable;

import com.timeScheduler.Activity.Activity;
import com.timeScheduler.Activity.ActivityRepository;
import com.timeScheduler.Activity.ActivityService;
import com.timeScheduler.Classroom.Classroom;
import com.timeScheduler.Classroom.ClassroomRepository;
import com.timeScheduler.Classroom.ClassroomService;
import com.timeScheduler.Classroom.ClassroomServiceImpl;
import com.timeScheduler.Graph.Graph;
import com.timeScheduler.Graph.GraphCalculationService;
import com.timeScheduler.Subject.Subject;
import com.timeScheduler.Subject.SubjectRepository;
import com.timeScheduler.Teacher.Teacher;
import com.timeScheduler.Teacher.TeacherRepository;
import com.timeScheduler.Times.Times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * TimeTable creation and calculation service implementation.
 */
@Service
public class TimeTableServiceImpl implements TimeTableService {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ClassroomService classroomService;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private TimeTableRepository timeTableRepository;

    @Autowired
    private GraphCalculationService graphCalculationService;

    @Override
    public TimeTable calculateChosenTimeTable(String timeTableType) {
        TimeTable timeTable = new TimeTable();
        timeTable.setCreatedOn(new Date());
        List<Subject> subjectList = subjectRepository.findAll();
        List<Activity> activities = activityService.initiateActivitiesBySubjects(subjectList);
        activities.forEach(activity -> activity.setTimeTable(timeTable));
        Collections.sort(activities);
        Graph coloredGraph = new Graph(activities);

        switch (timeTableType) {
            case "sortedGreedy":
                coloredGraph.sortAdjacencies();
                graphCalculationService.greedyGraphColoring(coloredGraph);
                break;
            case "sortedBackTracking":
                coloredGraph.sortAdjacencies();
                graphCalculationService.backtrackingGraphColoring(coloredGraph);
                break;
            case "shuffleGreedy":
                Collections.shuffle(activities);
                coloredGraph.setActivities(activities);
                graphCalculationService.greedyGraphColoring(coloredGraph);
                break;
            case "shuffleBackTracking":
                Collections.shuffle(activities);
                coloredGraph.setActivities(activities);
                graphCalculationService.backtrackingGraphColoring(coloredGraph);
                break;
            default:
                return null;
        }

        classroomService.assignClassroomsToActivities(coloredGraph.getActivities());
        timeTable.setActivities(coloredGraph.getActivities());
        timeTableRepository.save(timeTable);
        assignTime(coloredGraph.getActivities());

        return timeTable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeTable calculateGreedyTimeTable() {

        TimeTable timeTable = new TimeTable();
        timeTable.setCreatedOn(new Date());
        List<Subject> subjectList = subjectRepository.findAll();
        List<Activity> activities = activityService.initiateActivitiesBySubjects(subjectList);
        activities.forEach(activity -> activity.setTimeTable(timeTable));

        Graph coloredGraph = new Graph(activities);

        graphCalculationService.greedyGraphColoring(coloredGraph);
        classroomService.assignClassroomsToActivities(coloredGraph.getActivities());
        timeTable.setActivities(coloredGraph.getActivities());
        timeTableRepository.save(timeTable);
        assignTime(coloredGraph.getActivities());

        return timeTable;
    }

    @Override
    public TimeTable calculateBackTrackingTimeTable() {
        TimeTable timeTable = new TimeTable();
        timeTable.setCreatedOn(new Date());
        List<Subject> subjectList = subjectRepository.findAll();
        List<Activity> activities = activityService.initiateActivitiesBySubjects(subjectList);
        activities.forEach(activity -> activity.setTimeTable(timeTable));

        Graph coloredGraph = new Graph(activities);

        graphCalculationService.backtrackingGraphColoring(coloredGraph);
        classroomService.assignClassroomsToActivities(coloredGraph.getActivities());
        timeTable.setActivities(coloredGraph.getActivities());
        timeTableRepository.save(timeTable);
        assignTime(coloredGraph.getActivities());

        return timeTable;
    }

    public void assignTime(List<Activity> activities) {
        HashMap<Times, List<Activity>> result = new HashMap<>();

        //usedTimes bus saugomi jau panaudoti laikai, kad nesikartotu, priskiriant laikus
        List<Times> usedTimes = new ArrayList<>();

        List<Integer> colors = new ArrayList<>();
        for (Activity activity : activities) {
            colors.add(activity.getTimeSlot());
        }

        List<Integer> distinctColors = colors.stream()
                .distinct()
                .collect(Collectors.toList());

        for (Integer distinctColor : distinctColors) {
            //formuojamas vienos spalvos dėstytojų sąrašas
            List<Teacher> listTeachers = new ArrayList<>();
            List<Activity> activities1 = new ArrayList<>();
            for (Activity activity : activities) {
//jeigu spalva i  sutampa su activities spalva formuojam masyvą activities1
//ir spalvos ilistTeachers
                if (distinctColor == activity.getTimeSlot()) {
                    listTeachers.add(activity.getSubject().getTeacher());
                    activities1.add(activity);
                }
            }
            //patikrinam ar visi spalvos i teachers gali tuo laiku dirbti
            //suformuojam bendrus times
            List<Times> times = new ArrayList<>();
            //tikrinimas reikalingas, jeigu spalva turi tik vieną dėstytoją
            if (listTeachers.size() > 1) {
                for (int c = 0; c < listTeachers.size() - 1; c++) {
                    for (int d = 1; d < listTeachers.size(); d++) {
                        times = listTeachers.get(c).getTimes().stream()
                                .filter(listTeachers.get(d).getTimes()
                                        ::contains).collect(Collectors.toList());
                    }
                }
            } else times = listTeachers.get(0).getTimes();

            //formuojam hashmap result is laiko ir jo activities
            for (int k = 0; k < times.size(); k++) {

                if (usedTimes.size() == 0) {
                    result.put(times.get(k), activities1);
                    for (int j = 0; j < activities1.size(); j++) {
                        activities1.get(j).setTime(times.get(k).getTime());
                        activityRepository.save(activities1.get(j));
                    }
                    usedTimes.add(times.get(k));
                    break;
                } else {
                    int sk = 0;
                    for (int v = 0; v < usedTimes.size(); v++) {
                        if (usedTimes.get(v).getTime() == (times.get(k).getTime())) {
                            sk++;
                        }

                    }
                    if (sk == 0) {
                        result.put(times.get(k), activities1);
                        for (int r = 0; r < activities1.size(); r++) {
                            activities1.get(r).setTime(times.get(k).getTime());
                            activityRepository.save(activities1.get(r));
                        }
                        usedTimes.add(times.get(k));
                        break;
                    }
                }

            }
        }
        //result - jau tvarkarastis
        printHashmap(result);
    }

    public void printHashmap(HashMap result) {

        Set set = result.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry) iterator.next();
            Times t = (Times) mentry.getKey();
            ArrayList<Activity> l = (ArrayList<Activity>) mentry.getValue();
            System.out.println();
            System.out.println("time yra: " + t.getTime());
            for (int i = 0; i < l.size(); i++)
                System.out.println("grupių skaičius " + l.get(i).getSubject().getGroups().size() + " " + l.get(i).getSubject().getCourseName() + " " + l.get(i).getSubject().getTeacher().getName() + " " + l.get(i).getClassroom().getName() + " " + l.get(i).getSubject().getGroups().get(0).getNumber());
        }
    }

    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    public void setClassroomService(ClassroomService classroomService) {
        this.classroomService = classroomService;
    }

    public void setSubjectRepository(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    public void setTimeTableRepository(TimeTableRepository timeTableRepository) {
        this.timeTableRepository = timeTableRepository;
    }

    public void setGraphCalculationService(GraphCalculationService graphCalculationService) {
        this.graphCalculationService = graphCalculationService;
    }

    public void setActivityRepository(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }
}
