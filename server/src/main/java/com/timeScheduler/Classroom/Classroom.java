package com.timeScheduler.Classroom;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.timeScheduler.Activity.Activity;

import javax.persistence.*;
import java.util.List;

/**
 * Entity class that represents Classroom.
 */
@Entity
public class Classroom {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private int capacity;

    private String type;

    public Classroom() {
    }

    public Classroom(String name, String type, int capacity) {
        this.name = name;
        this.type = type;
        this.capacity = capacity;

    }

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "classroom_id")
    @JsonBackReference
    private List<Activity> activities;

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
