package com.timeScheduler.Classroom;

import com.timeScheduler.Activity.Activity;

import java.util.List;

public interface ClassroomService {

    /**
     * Assigns classrooms for activities by the time slot.
     *
     * @param activities for which classrooms will be assigned
     */
    List<Activity>  assignClassroomsToActivities(List<Activity> activities);
}
