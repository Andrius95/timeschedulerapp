package com.timeScheduler.Classroom;

import com.timeScheduler.Activity.Activity;
import com.timeScheduler.Activity.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClassroomServiceImpl implements ClassroomService {

    @Autowired
    private ClassroomRepository classroomRepository;
    @Autowired
    private ActivityRepository activityRepository;
    /**
     * {@inheritDoc}
     */
    @Override
    public List <Activity> assignClassroomsToActivities(List<Activity> activities) {
        List<Classroom> allClassroomsList = classroomRepository.findAll();
        List<Activity> activityList = activityRepository.findAll();
        List<Classroom> teorclassList = new ArrayList<>();
        List<Classroom> practclassList = new ArrayList<>();
        Iterator<Classroom> iterator = allClassroomsList.iterator();
        Classroom a;
        while (iterator.hasNext()) {
            a = iterator.next();
            if (a.getType().contains("teorinė"))
                teorclassList.add(a);
            else if (a.getType().contains("praktinė"))
                practclassList.add(a);
        }
       //  System.out.println("teor kl" + teorclassList.get(0).getName()+teorclassList.size() + "prakt kl" + practclassList.size());
        List <Integer> colors = new ArrayList<>();
        for (int j = 0; j < activities.size(); j++) {
            colors.add(activities.get(j).getTimeSlot());
        }

        List<Integer> distinctColors = colors.stream()
                .distinct()
                .collect(Collectors.toList());

//        System.out.println();
//        System.out.println("spalvų skaičius "+distinctColors.size());
        //timeslot kol kas visi -1
        for (int i = 0; i < distinctColors.size(); i++) {

            int tc = 0, pc = 0;
            for (int j = 0; j < activities.size(); j++) {
                if (i == activities.get(j).getTimeSlot()) {
                    if (activities.get(j).getSubject().getClassroomType()== "teorinė") {
                        activities.get(j).setClassroom(teorclassList.get(tc++));
                    } else {
                        activities.get(j).setClassroom(practclassList.get(pc++));
                    }
              //    activityRepository.save(activities.get(j));
                }
            }
        }
        return activities;
    }
    public ClassroomRepository getClassroomRepository() {
        return classroomRepository;
    }

    public void setClassroomRepository(ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }
}
