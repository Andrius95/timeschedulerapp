package com.timeScheduler.Classroom;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RequestMapping("/classroom")
@RestController
public class ClassroomController {

    private ClassroomRepository repository;


    @Autowired
    private ClassroomService activityService;

    public ClassroomController(ClassroomRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/all-classrooms")
    @CrossOrigin(origins = "http://localhost:3000")
    public Collection<Classroom> allClassrooms() {

        List<Classroom> classrooms = repository.findAll();

        return classrooms;
    }
}

