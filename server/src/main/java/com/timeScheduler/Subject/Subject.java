package com.timeScheduler.Subject;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.timeScheduler.Activity.Activity;
import com.timeScheduler.Group.Group;
import com.timeScheduler.Teacher.Teacher;

import javax.persistence.*;
import java.util.List;

/**
 * Entity class that represents Subject.
 */
@Entity
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="teacher_id")
    private Teacher teacher;

    private String courseName;

    @ManyToMany(mappedBy = "subjects")
    private List<Group> groups;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id")
    @JsonBackReference
    private List<Activity> activities;

    private int timesPerWeek;

    private String classroomType;

    public Subject(){

    }
    public Subject(String courseName, int timesPerWeek, String classroomType) {
        this.courseName = courseName;
        this.timesPerWeek = timesPerWeek;
        this.classroomType = classroomType;
    }

    public String getClassroomType() {
        return classroomType;
    }

    public void setClassroomType(String classroomType) {
        this.classroomType = classroomType;
    }

    public int getTimesPerWeek() {
        return timesPerWeek;
    }

    public void setTimesPerWeek(int timesPerWeek) {
        this.timesPerWeek = timesPerWeek;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
