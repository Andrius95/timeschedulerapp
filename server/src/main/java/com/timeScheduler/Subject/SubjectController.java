package com.timeScheduler.Subject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequestMapping("/subject")
@RestController
public class SubjectController {

    private SubjectRepository repository;

    @Autowired
    private SubjectService subjectService;

    public SubjectController(SubjectRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/all-subjects")
    @CrossOrigin(origins = "*")
    public Collection<Subject> allSubjects() {
        return repository.findAll();
    }

    @DeleteMapping("/delete-subject/{id}")
    @CrossOrigin(origins = "*")
    public void deleteSubject(@PathVariable Long id) {
        repository.deleteById(id);
    }

    @PostMapping("/add-subject")
    @CrossOrigin(origins = "*")
    public void addSubject(@RequestBody Subject subject) {
        repository.save(subject);
    }

    @GetMapping("/all-free-subjects")
    @CrossOrigin(origins = "*")
    public Collection<Subject> allFreeSubjects() {
        return subjectService.findAllFreeSubjects();
    }

    public SubjectService getSubjectService() {
        return subjectService;
    }

    public void setSubjectService(SubjectService subjectService) {
        this.subjectService = subjectService;
    }
}


