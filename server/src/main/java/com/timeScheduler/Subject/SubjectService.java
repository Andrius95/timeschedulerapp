package com.timeScheduler.Subject;

import java.util.List;

public interface SubjectService {

    /**
     * Finds All subjects that does not have assigned teacher to it.
     *
     * @return list of {@link Subject} that is without assigned teacher
     */
    List<Subject> findAllFreeSubjects();
}
