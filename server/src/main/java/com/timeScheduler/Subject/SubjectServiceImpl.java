package com.timeScheduler.Subject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Subject Service Implementation.
 */
@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectRepository repository;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Subject> findAllFreeSubjects() {
        List<Subject> allSubjects = repository.findAll();
        if (!allSubjects.isEmpty()) {
            return allSubjects.stream()
                    .filter(subject -> subject.getTeacher() == null)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public SubjectRepository getRepository() {
        return repository;
    }

    public void setRepository(SubjectRepository repository) {
        this.repository = repository;
    }
}
