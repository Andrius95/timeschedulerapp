package com.timeScheduler.Graph;

import com.timeScheduler.Activity.Activity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Service
public class GraphCalculationServiceImpl implements GraphCalculationService {

    /**
     * {@inheritDoc}
     */
    @Override
    public void greedyGraphColoring(Graph graph) {
        List<Activity> activities = graph.getActivities();
        int verticiesLength = graph.getVerticiesLength();

        // Assign the first color to first vertex
        activities.get(0).setTimeSlot(0);

        // A temporary array to store the available colors. False
        // value of available[cr] would mean that the color cr is
        // assigned to one of its adjacent vertices
        boolean available[] = new boolean[verticiesLength];

        // Initially, all colors are available
        Arrays.fill(available, true);
        System.out.println("greedy spalvos");
        // Assign colors to remaining V-1 vertices
        for (int u = 1; u < verticiesLength; u++) {
            // Process all adjacent vertices and flag their colors
            // as unavailable
            Iterator it = graph.getAdjacencies()[u].iterator();
            while (it.hasNext()) {
                Activity i = (Activity) it.next();
                if (i.getTimeSlot() != -1)
                    available[i.getTimeSlot()] = false;

            }

            // Find the first available color
            int cr;
            for (cr = 0; cr < verticiesLength; cr++) {
                if (available[cr])
                    break;
            }
            // Assign the found color
            activities.get(u).setTimeSlot(cr);

            // Reset the values back to true for the next iteration
            Arrays.fill(available, true);
        }

        System.out.println();
        for (int i = 0; i < activities.size(); i++) {
            System.out.print("spalva  " + (activities.get(i).getTimeSlot()));
            System.out.print("  " + activities.get(i).getSubject().getCourseName() + " "+ activities.get(i).getSubject().getTeacher().getName()+" ");
            System.out.println();
        }
        System.out.println();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void backtrackingGraphColoring(Graph graph) {
        List<Activity> graphActivities = graph.getActivities();
        int[][] activitiesConflictMatrix = graph.getGraphConflictMatrix();
        int vertexLength = graphActivities.size();
        // Initialize all color values as -1.
        int[] color = new int[vertexLength];
        for (int i = 0; i < vertexLength; i++) {
            color[i] = -1;
        }

        // Call graphColoring() for vertex 0
        if (!graphColoring(activitiesConflictMatrix, vertexLength, color, 0)) {
            System.out.println("Sprendimas neegzistuoja");
        }

        // Assign calculated colors to activities
        for (int i = 0; i < vertexLength; i++) {
            graphActivities.get(i).setTimeSlot(color[i]);
        }

        // Print the solution
        printSolution(color, graphActivities, vertexLength);
    }

    private boolean graphColoring(int[][] conflictMatrix, int vertexLength,
                                  int[] color, int activeVertexIndex) {
        /* If all vertices are assigned
           a color then return true */
        if (activeVertexIndex == vertexLength) {
            return true;
        }

        /* Consider this vertex v and try different
           colors */
        for (int assigningColor = 0; assigningColor < vertexLength; assigningColor++) {
            if (isPossible(activeVertexIndex, conflictMatrix, color, assigningColor, vertexLength)) {
                color[activeVertexIndex] = assigningColor;

                /*
                Try to assign colors to next (activeVertexIndex + 1) vertex,
                by recursion.
                 */
                if (graphColoring(conflictMatrix, vertexLength,
                        color, activeVertexIndex + 1)) {
                    return true;
                }

                /* If assigningColor doesn't lead
                   to a solution then remove it */
                color[activeVertexIndex] = -1;
            }
        }

        /* If no color can be assigned to this vertex
           then return false */
        return false;
    }

    /**
     * Checks whether colorToAssign can be assigned
     * to given vertex color[activeVertexIndex].
     *
     * @param activeVertexIndex index of vertex we try to assign color
     * @param conflictMatrix    used to check vertex conflicts
     * @param color             array of all colors
     * @param colorToAssign     color we are trying to assign for vertex
     * @param vertexLength
     * @return {@code true} if value is assignable to vertex, {@code false} otherwise
     */
    private boolean isPossible(int activeVertexIndex, int[][] conflictMatrix,
                               int[] color,
                               int colorToAssign, int vertexLength) {
        for (int i = 0; i < vertexLength; i++) {
            if (conflictMatrix[activeVertexIndex][i] == 1 && colorToAssign == color[i]) {
                return false;
            }
        }
        return true;
    }

    private void printSolution(int[] color, List<Activity> activities, int V) {
        System.out.println();
        System.out.println("Sprendimas egzistuoja: Backtraking algoritmo metu sugeneruotos spalvos");
        for (int i = 0; i < V; i++) {
            System.out.print(" spalva  " + color[i] + " ");
            System.out.print(" " + activities.get(i).getSubject().getCourseName() + " "+ activities.get(i).getSubject().getTeacher().getName()+" ");
            System.out.println();
        }

        System.out.println();
    }
}

