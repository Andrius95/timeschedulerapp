package com.timeScheduler.Graph;

import com.timeScheduler.Activity.Activity;
import com.timeScheduler.Group.Group;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of graph.
 */
public class Graph {

    /**
     * Number of vertices.
     */
    private int verticiesLength;

    /**
     * Adjacencies list.
     */
    private LinkedList[] adjacencies;

    /**
     * Activities list of graph
     */
    private List<Activity> activities;

    /**
     * Coloured graph initialization, using given activities.
     */
    public Graph(List<Activity> activities) {
        verticiesLength = activities.size();
        this.activities = activities;
        adjacencies = new LinkedList[verticiesLength];
        for (int i = 0; i < verticiesLength; i++) {
            adjacencies[i] = new LinkedList<Activity>();
        }
        resolveGraphEdges();
    }

    /**
     * Sorts activities, from first highest conflict number vertex (desc) order.
     */
    public void sortAdjacencies() {
        // Collects number of all vertex conflicts, sorts it and remove duplicates.
        List<Integer> vertexConflictsList = new ArrayList<>();
        for (LinkedList adjacency : adjacencies) {
            vertexConflictsList.add(adjacency.size());
        }
        vertexConflictsList = vertexConflictsList.stream()
                .sorted(Comparator.reverseOrder())
                .distinct()
                .collect(Collectors.toList());

        List<Activity> sortedActivities = new ArrayList<>();
        LinkedList[] sortedAdjacencies = new LinkedList[verticiesLength];

        // Updates graph activities and adjecencies with order.
        int k = 0;
        for (int j = 0; j < vertexConflictsList.size(); j++) {
            for (int i = 0; i < activities.size(); i++) {
                if (adjacencies[i].size() == vertexConflictsList.get(j)) {
                    sortedActivities.add(activities.get(i));
                    sortedAdjacencies[k] = adjacencies[i];
                    k++;
                }
            }
        }
        this.adjacencies = sortedAdjacencies;
        this.activities = sortedActivities;
    }

    /**
     * Creates adjacencies between activities, if activities share same
     * student group or teacher.
     */
    private void resolveGraphEdges() {
        List<Group> groups;
        for (int i = 0; i < verticiesLength - 1; i++) {
            for (int j = i + 1; j < verticiesLength; j++) {
                if (activities.get(i).getSubject().getTeacher() == activities.get(j).getSubject().getTeacher()) {
                    addEdge(activities.get(i), activities.get(j), i, j);
                } else {
                    groups = activities.get(i).getSubject().getGroups().stream()
                            .filter(activities.get(j).getSubject().getGroups()
                                    ::contains)
                            .collect(Collectors.toList());
                    if (!groups.isEmpty()) {
                        addEdge(activities.get(i), activities.get(j), i, j);
                    }
                }
            }
        }
    }

    /**
     * Connects activities in adjacencies list.
     *
     * @param a1 first activity to connect
     * @param a2 second activity to connect
     * @param i  first activity index in adjacencies list
     * @param j  second activity index in adjacencies list
     */
    private void addEdge(Activity a1, Activity a2, int i, int j) {
        adjacencies[i].add(a2);
        adjacencies[j].add(a1);
    }

    public int[][] getGraphConflictMatrix() {
        int[][] conflictMatrix = new int[activities.size()][activities.size()];
        System.out.println("gretimumo matrica: ");
        for (int i = 0; i < activities.size(); i++) {
            System.out.println(activities.get(i).getSubject().getCourseName());
            for (int k = 0; k < activities.size(); k++) {
                if (adjacencies[i].contains(activities.get(k)))
                    conflictMatrix[i][k] = 1;
                else conflictMatrix[i][k] = 0;
                System.out.print(conflictMatrix[i][k] + " ");
            }
            System.out.println();
        }
        return conflictMatrix;
    }

    public int getVerticiesLength() {
        return verticiesLength;
    }

    public LinkedList[] getAdjacencies() {
        return adjacencies;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
        resolveGraphEdges();
    }
}
