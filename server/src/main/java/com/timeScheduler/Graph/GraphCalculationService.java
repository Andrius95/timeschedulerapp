package com.timeScheduler.Graph;

import com.timeScheduler.Activity.Activity;

import java.util.List;

public interface GraphCalculationService {

    /**
     * Calculates all the time slots for activities using greedy graph coloring algorithm.
     *
     * @param graph which activities time slots will be calculated
     */
    void greedyGraphColoring(Graph graph);

    /**
     * Calculates all the time slots for activities using backtracking graph coloring algorithm.
     *
     * @param graph which activities time slots will be calculated
     */
    void backtrackingGraphColoring(Graph graph);
}
