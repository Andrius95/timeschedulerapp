package com.timeScheduler;

import com.timeScheduler.Activity.ActivityRepository;
import com.timeScheduler.Address.Address;
import com.timeScheduler.Classroom.Classroom;
import com.timeScheduler.Classroom.ClassroomRepository;
import com.timeScheduler.Course.Course;
import com.timeScheduler.Course.CourseRepository;
import com.timeScheduler.Group.Group;
import com.timeScheduler.Subject.Subject;
import com.timeScheduler.Subject.SubjectRepository;
import com.timeScheduler.Teacher.Teacher;
import com.timeScheduler.Teacher.TeacherRepository;
import com.timeScheduler.TeacherInfo.TeacherInfo;
import com.timeScheduler.Times.Times;
import com.timeScheduler.Times.TimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataInit implements ApplicationRunner {

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private ClassroomRepository classroomRepository;

    @Autowired
    private TimeRepository timeRepository;
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private SubjectRepository subjectRepository;

    public DataInit() {
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<Times> t = new ArrayList<>();
        List<Times> times2 = new ArrayList<>();
        for (int i = 0; i < 25; i++) {
            Times t1 = new Times();
            t1.setTime(i + 1);
            t.add(t1);

        }
        for (int i = 0; i < 25; i = i + 5) {
            t.get(i).setHours("9.00:10.30");
            t.get(i + 1).setHours("10.40:12.10");
            t.get(i + 2).setHours("12.55:14.25");
            t.get(i + 3).setHours("14.32:16.05");
            t.get(i + 4).setHours("16.15:17.45");
        }
        for (int i = 0; i < 5; i++) {
            t.get(i).setWeekDay("Pirmadienis");
        }
        for (int i = 5; i < 10; i++) {
            t.get(i).setWeekDay("Antradienis");
        }
        for (int i = 10; i < 15; i++) {
            t.get(i).setWeekDay("Trečiadienis");
        }
        for (int i = 15; i < 20; i++) {
            t.get(i).setWeekDay("Ketvirtadienis");
        }
        for (int i = 20; i < 25; i++) {
            t.get(i).setWeekDay("Penktadienis");
        }
        //--------------------1 pavyzdys--------------------------//
        Course course = new Course();
        course.setName("Informacinės sistemos");

        Group group1 = new Group();
        group1.setNumber(1);
        group1.setCourse(course);
        Subject s1 = new Subject("Anglų k.", 1, "teorinė");
        List<Group> firstSubjectGroups = new ArrayList<>();
        firstSubjectGroups.add(group1);
        //   s1.setGroups(firstSubjectGroups);
        Subject s2 = new Subject("Duomenų bazės", 2, "praktinė");
        Subject s3 = new Subject("Matematika", 1, "teorinė");
        Subject s4 = new Subject("Kompiuterių architektūra ir tinklai", 1, "praktinė");
        Subject s5 = new Subject("Informacinės sistemos", 2, "praktinė");
        Subject s6 = new Subject("Aplinkos ir žmonių sauga", 1, "teorinė");
        Subject s7 = new Subject("Operacinės sistemos", 1, "praktinė");
        // s7.setGroups(firstSubjectGroups);
        Subject s8 = new Subject("Programavimo pagrindai", 2, "praktinė");
        Subject s9 = new Subject("Verslo pagrindai", 1, "praktinė");
        Subject s10 = new Subject("Duomenų sauga", 2, "praktinė");
        Subject s11 = new Subject("Duomenų struktūros ir algoritmai", 1, "praktinė");
        Subject s12 = new Subject("Internetinės sistemos", 2, "praktinė");
        Subject s13 = new Subject("Internetinių sistemų praktika", 2, "praktinė");
        Subject s14 = new Subject("Verslo informacinės sistemos", 2, "praktinė");
        Subject s15 = new Subject("Kompiuterinių sistemų projektavimas", 2, "praktinė");
        Subject s16 = new Subject("Virtualizacija", 2, "praktinė");
        Subject s17 = new Subject("Kompiuterinė grafika", 1, "praktinė");

        List<Subject> firstGroupSubjects = new ArrayList<>();
        firstGroupSubjects.add(s1);
        firstGroupSubjects.add(s2);
        firstGroupSubjects.add(s4);
        firstGroupSubjects.add(s3);
        firstGroupSubjects.add(s7);
        firstGroupSubjects.add(s8);
        List<Subject> secondGroupSubjects = new ArrayList<>();
        // secondGroupSubjects.add(s4);
        secondGroupSubjects.add(s5);
        secondGroupSubjects.add(s6);
        secondGroupSubjects.add(s9);
        secondGroupSubjects.add(s10);
        secondGroupSubjects.add(s11);
        //dalykai bendri dviems grupems
        secondGroupSubjects.add(s7);
        secondGroupSubjects.add(s8);
        List<Subject> thirdGroupSubjects = new ArrayList<>();
        thirdGroupSubjects.add(s12);
        thirdGroupSubjects.add(s13);
        thirdGroupSubjects.add(s14);
        thirdGroupSubjects.add(s15);
        thirdGroupSubjects.add(s16);
        thirdGroupSubjects.add(s17);
        //  s8.setGroups(secondSubjectGroups);
        // Teachers gauna subjects
        Teacher t1 = new Teacher();
        t1.setName("lekt. T1");
        List<Subject> firstTeacherSubjects = new ArrayList<>();
        firstTeacherSubjects.add(s1);
        // firstTeacherSubjects.add(s4);
        t1.setSubjects(firstTeacherSubjects);
        Teacher t2 = new Teacher();
        t2.setName("lekt. T2");
        List<Subject> secondTeacherSubjects = new ArrayList<>();
        secondTeacherSubjects.add(s2);
        secondTeacherSubjects.add(s11);
        t2.setSubjects(secondTeacherSubjects);
        Teacher t3 = new Teacher();
        t3.setName("doc. T3");
        List<Subject> thirdTeacherSubjects = new ArrayList<>();
        thirdTeacherSubjects.add(s3);
        //thirdTeacherSubjects.add(s6);
        t3.setSubjects(thirdTeacherSubjects);
        Teacher t4 = new Teacher();
        t4.setName("asist. T4");
        List<Subject> fourthTeacherSubjects = new ArrayList<>();
        fourthTeacherSubjects.add(s4);
        t4.setSubjects(fourthTeacherSubjects);
        Teacher t5 = new Teacher();
        t5.setName("doc. T5");
        List<Subject> fifthTeacherSubjects = new ArrayList<>();
        fifthTeacherSubjects.add(s8);
        fifthTeacherSubjects.add(s12);
        fifthTeacherSubjects.add(s13);
        t5.setSubjects(fifthTeacherSubjects);
        Teacher t6 = new Teacher();
        t6.setName("lekt. T6");
        List<Subject> sixthTeacherSubjects = new ArrayList<>();
        sixthTeacherSubjects.add(s7);
        t6.setSubjects(sixthTeacherSubjects);
        Teacher t7 = new Teacher();
        t7.setName("lekt. T7");
        List<Subject> seventhTeacherSubjects = new ArrayList<>();
        seventhTeacherSubjects.add(s5);
        seventhTeacherSubjects.add(s15);
        t7.setSubjects(seventhTeacherSubjects);
        Teacher t8 = new Teacher();
        t8.setName("lekt. T8");
        List<Subject> eigthTeacherSubjects = new ArrayList<>();
        eigthTeacherSubjects.add(s6);
        t8.setSubjects(eigthTeacherSubjects);
        Teacher t9 = new Teacher();
        t9.setName("lekt. T9");
        List<Subject> ninethTeacherSubjects = new ArrayList<>();
        ninethTeacherSubjects.add(s9);
        t9.setSubjects(ninethTeacherSubjects);
        Teacher t10 = new Teacher();
        t10.setName("lekt. T10");
        List<Subject> tenthTeacherSubjects = new ArrayList<>();
        tenthTeacherSubjects.add(s10);
        tenthTeacherSubjects.add(s16);
        t10.setSubjects(tenthTeacherSubjects);
        Teacher t11 = new Teacher();
        t11.setName("lekt. T11");
        List<Subject> eleventhTeacherSubjects = new ArrayList<>();
        eleventhTeacherSubjects.add(s14);
        t11.setSubjects(eleventhTeacherSubjects);
        Teacher t12 = new Teacher();
        t12.setName("lekt. T12");

        List<Subject> twelfthTeacherSubjects = new ArrayList<>();
        twelfthTeacherSubjects.add(s17);
        t12.setSubjects(twelfthTeacherSubjects);
        s1.setTeacher(t1);
        s2.setTeacher(t2);
        s3.setTeacher(t3);

        group1.setSubjects(firstGroupSubjects);

        Group group2 = new Group();
        group2.setNumber(2);
        group2.setCourse(course);
        Group group3 = new Group();
        group3.setNumber(3);
        group3.setCourse(course);

        List<Group> groups = new ArrayList<Group>();
        groups.add(group1);
        groups.add(group2);
        groups.add(group3);
        course.setGroups(groups);

        s4.setTeacher(t4);
        s5.setTeacher(t7);
        s6.setTeacher(t8);
        s7.setTeacher(t6);
        s8.setTeacher(t5);
        s9.setTeacher(t9);
        s10.setTeacher(t10);
        s11.setTeacher(t2);
        s12.setTeacher(t5);
        s13.setTeacher(t5);
        s14.setTeacher(t11);
        s15.setTeacher(t7);
        s16.setTeacher(t10);
        s17.setTeacher(t12);
        group2.setSubjects(secondGroupSubjects);
        group3.setSubjects(thirdGroupSubjects);
        List<Times> alltimes = timeRepository.findAll();
        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(t1);
        teacherList.add(t2);
        teacherList.add(t3);
        teacherList.add(t4);
        teacherList.add(t5);
        teacherList.add(t6);
        teacherList.add(t7);
        teacherList.add(t8);
        teacherList.add(t9);
        teacherList.add(t10);
        teacherList.add(t11);
        teacherList.add(t12);
        t1.setTimes(t);
        t2.setTimes(t);
        t3.setTimes(t);
        t4.setTimes(t);
        t5.setTimes(t);
        t6.setTimes(t);
        t7.setTimes(t);
        t8.setTimes(t);
        t9.setTimes(t);
        t10.setTimes(t);
        t11.setTimes(t);
        t12.setTimes(t);
        for (int i = 5; i < t.size() - 5; i++)
            times2.add(t.get(i));
// dėstytojui t5 uždedamas laikas tik nuo antradienio iki ketvirtadienio
        t5.setTimes(times2);
        t6.setTimes(times2);
        List<Times> times3 = new ArrayList<>();
        for (int i = 10; i < t.size() - 5; i++)
            times3.add(t.get(i));
        List<Times> times4 = new ArrayList<>();
        for (int i = 15; i < t.size(); i++)
            times4.add(t.get(i));
        // dėstytojui t3 uždedamas laikas tik nuo trečiadienio
        t3.setTimes(times3);
        t8.setTimes(times3);
        //  t10.setTimes(times3);
        t11.setTimes(times3);
        //   t11.setTimes(times4);
        t2.setTimes(times4);

//--------------2 testinis variantas--------------------------------//
//
//        Course course = new Course();
//        Course course1 = new Course();
//        Course course2 = new Course();
//        course.setName("Pirmas kursas");
//        course1.setName("Antras kursas");
//        course2.setName("Tre2ias kursas");
//
//        Group group1 = new Group();
//        group1.setNumber(1);
//        group1.setCourse(course);
//
//        List<Group> firstSubjectGroups = new ArrayList<>();
//        firstSubjectGroups.add(group1);
//
//        //     Subject s1 = new Subject();
//        Subject s1 = new Subject("Anglų k.", 1, "teorinė");
//        s1.setGroups(firstSubjectGroups);
//
//        Subject s2 = new Subject("Rusų k.", 1, "teorinė");
//        s2.setGroups(firstSubjectGroups);
//
//        Subject s3 = new Subject("Duomenų struktūros", 1, "teorinė");
//        s3.setGroups(firstSubjectGroups);
////2
//        Subject s4 = new Subject("Duomenų analizė", 1, "teorinė");
//        Subject s5 = new Subject("Teisės pagrindai", 1, "teorinė");
//        Subject s6 = new Subject("Taikomoji matematika", 1, "teorinė");
//        //2
//        Subject s7 = new Subject("Operacinės sistemos", 1, "praktinė");
//        s7.setGroups(firstSubjectGroups);
//        //2
//        Subject s8 = new Subject("Programavimas", 1, "praktinė");
//
//        List<Subject> firstGroupSubjects = new ArrayList<>();
//        firstGroupSubjects.add(s1);
//        firstGroupSubjects.add(s2);
//        firstGroupSubjects.add(s3);
//        firstGroupSubjects.add(s7);
//        firstGroupSubjects.add(s6);//dalykas abiejose grupėse
//
//        group1.setSubjects(firstGroupSubjects);
//
//        //-------------------------------------------------------------------------------------------------------//
//
//        Group group2 = new Group();
//        group2.setNumber(2);
//        group2.setCourse(course);
//
//        List<Subject> secondGroupSubjects = new ArrayList<>();
//        secondGroupSubjects.add(s4);
//        secondGroupSubjects.add(s5);
//        secondGroupSubjects.add(s6);
//        secondGroupSubjects.add(s8);
//
//        group2.setSubjects(secondGroupSubjects);
//
//        List<Group> secondSubjectGroups = new ArrayList<>();
//        s4.setGroups(secondSubjectGroups);
//        s5.setGroups(secondSubjectGroups);
//        s6.setGroups(secondSubjectGroups);
//        s6.setGroups(firstSubjectGroups);//pasikartojanti grupe, bendras dalykas
//        s8.setGroups(secondSubjectGroups);
//
//        //-------------------------------------------------------------------------------------------------------//
//
//        // Teachers gauna subjects
//        Teacher t1 = new Teacher();
//        t1.setName("Dėstytojas 1");
//     //   List<Integer> timest1 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
//
//        List<Subject> firstTeacherSubjects = new ArrayList<>();
//        firstTeacherSubjects.add(s1);
//        firstTeacherSubjects.add(s4);
//        t1.setSubjects(firstTeacherSubjects);
//        t1.setTimes(t);
//        Teacher t2 = new Teacher();
//        t2.setName("Dėstytojas 2");
//        //  t2.setTimes(timest1);
//        List<Subject> secondTeacherSubjects = new ArrayList<>();
//        secondTeacherSubjects.add(s2);
//        secondTeacherSubjects.add(s5);
//        t2.setSubjects(secondTeacherSubjects);
//        t2.setTimes(t);
//        Teacher t3 = new Teacher();
//        t3.setName("Dėstytojas 3");
//       //    t3.setTimes(t);
//        List<Subject> thirdTeacherSubjects = new ArrayList<>();
//        thirdTeacherSubjects.add(s3);
//        thirdTeacherSubjects.add(s6);
//        t3.setSubjects(thirdTeacherSubjects);
//        Teacher t4 = new Teacher();
//        t4.setName("Dėstytojas 4");
//          t4.setTimes(t);
//        List<Subject> fourthTeacherSubjects = new ArrayList<>();
//        fourthTeacherSubjects.add(s7);
//        t4.setSubjects(fourthTeacherSubjects);
//        Teacher t5 = new Teacher();
//        t5.setName("Dėstytojas 5");
//for (int i=5;i<t.size()-5;i++)
//    times2.add(t.get(i));
//// dėstytojui t5 uždedamas laikas tik nuo antradienio iki ketvirtadienio
//        t5.setTimes(times2);
//        List<Times> times3=new ArrayList<>();
//        for (int i = 10; i<t.size()-5; i++)
//            times3.add(t.get(i));
//        // dėstytojui t3 uždedamas laikas tik nuo trečiadienio
//        t3.setTimes(times3);
//        List<Subject> fifthTeacherSubjects = new ArrayList<>();
//        fifthTeacherSubjects.add(s8);
//        t5.setSubjects(fifthTeacherSubjects);
//        List<Times> times11 = new ArrayList<>();
////
//      List<Times> alltimes = timeRepository.findAll();
//        List<Teacher> teacherList = new ArrayList<>();
//        teacherList.add(t1);
//        teacherList.add(t2);
//        teacherList.add(t3);
//        teacherList.add(t4);
//        teacherList.add(t5);
//
//      //  List <Teacher> allTeachers= teacherRepository.findAll();
//      //  List <Teacher> teacherList5=new ArrayList<>();
//
//      //  teacherList5.add(t5);
//
//        s1.setTeacher(t1);
//        s2.setTeacher(t2);
//        s3.setTeacher(t3);
//
//        List<Group> groups = new ArrayList<>();
//        groups.add(group1);
//        groups.add(group2);
//        course.setGroups(groups);
//
//        s4.setTeacher(t1);
//        s5.setTeacher(t2);
//        s6.setTeacher(t3);
//        s7.setTeacher(t4);
//        s8.setTeacher(t5);
        //--------------------------------------------------------------//
        populateTeacherInfoAndAddress(teacherList);
        for (int i = 0; i < t.size(); i++) {
            List<Teacher> oneTimesTeacherList = new ArrayList<>();
            for (int j = 0; j < teacherList.size(); j++) {
                {
                    for (int c = 0; c < teacherList.get(j).getTimes().size(); c++)
                        if (teacherList.get(j).getTimes().get(c).getTime() == t.get(i).getTime())
                        // if (teacherList.get(j).getTimes().contains(t.get(i).getTime()))
                        {
                            oneTimesTeacherList.add(teacherList.get(j));
                        }
                }

            }
            t.get(i).setTeachers(oneTimesTeacherList);

        }

        courseRepository.save(course);
        List<Classroom> classrooms = new ArrayList<>();
        Classroom classroom1 = new Classroom("1 audit.", "praktinė", 20);
        Classroom classroom2 = new Classroom("2 audit.", "praktinė", 20);
        Classroom classroom3 = new Classroom("3 audit.", "praktinė", 20);
        Classroom classroom4 = new Classroom("4 audit.", "praktinė", 20);
        Classroom classroom5 = new Classroom("5 audit.", "teorinė", 20);
        Classroom classroom6 = new Classroom("6 audit.", "teorinė", 20);
        Classroom classroom7 = new Classroom("7 audit.", "teorinė", 20);
        Classroom classroom8 = new Classroom("8 audit.", "teorinė", 20);
        //auditorijų išsaugojimas į DB ---------
        classrooms.add(classroom1);
        classrooms.add(classroom2);
        classrooms.add(classroom3);
        classrooms.add(classroom4);
        classrooms.add(classroom5);
        classrooms.add(classroom6);
        classrooms.add(classroom7);
        classrooms.add(classroom8);
        for (int j = 0; j < classrooms.size(); j++) {
            classroomRepository.save(classrooms.get(j));
        }

    }

    private void populateTeacherInfoAndAddress(List<Teacher> teachersToPopulate) {
        TeacherInfo teacherInfo = new TeacherInfo();
        teacherInfo.setFirstName("Teacher");
        teacherInfo.setFirstName("Test");
        Address address = new Address();
        address.setAddressLine1("Test address line 1");
        teacherInfo.setAddress(address);
        address.setTeacherInfo(teacherInfo);
        teachersToPopulate.forEach(teacher -> teacher.setTeacherInfo(teacherInfo));
    }

    public CourseRepository getCourseRepository() {
        return courseRepository;
    }

    public void setCourseRepository(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public ClassroomRepository getClassroomRepository() {
        return classroomRepository;
    }

    public void setClassroomRepository(ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }

    public TimeRepository getTimeRepository() {
        return timeRepository;
    }

    public void setTimeRepository(TimeRepository timeRepository) {
        this.timeRepository = timeRepository;
    }

    public TeacherRepository getTeacherRepository() {
        return teacherRepository;
    }

    public void setTeacherRepository(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public ActivityRepository getActivityRepository() {
        return activityRepository;
    }

    public void setActivityRepository(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public SubjectRepository getSubjectRepository() {
        return subjectRepository;
    }

    public void setSubjectRepository(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }
}
