package com.timeScheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimeSchedulerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimeSchedulerApplication.class, args);
	}

}
