package com.timeScheduler.Group;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequestMapping("/group")
@RestController
public class GroupController {
    private GroupRepository repository;

    public GroupController(GroupRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/all-groups")
    @CrossOrigin(origins = "http://localhost:3000")
    public Collection<Group> allGroups() {
        return repository.findAll();
    }
}