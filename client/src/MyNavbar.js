import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

class MyNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.onTabChange = this.onTabChange.bind(this);
  }

  onTabChange(newTab) {
    this.props.activeTab(newTab);
  }

  render() {
    return (
      <Navbar variant="light" bg="light">
        <Navbar.Brand href="#">Pradinis</Navbar.Brand>
        <Nav.Link onClick={() => this.onTabChange("Teachers")}>
          Dėstytojai
        </Nav.Link>
        <Nav.Link onClick={() => this.onTabChange("StudentGroups")}>
          Studentų grupės
        </Nav.Link>
        <Nav.Link onClick={() => this.onTabChange("Subjects")}>
          Dalykai
        </Nav.Link>
        <Nav.Link onClick={() => this.onTabChange("Activities")}>
          Tvarkaraštis
        </Nav.Link>
        <Nav.Link onClick={() => this.onTabChange("Teachersactivities")}>
          Dėstytojų užimtumas
        </Nav.Link>
        <Nav.Link onClick={() => this.onTabChange("Roomsactivities")}>
          Auditorijų apkrovimas
        </Nav.Link>
      </Navbar>
    );
  }
}

export default MyNavbar;
