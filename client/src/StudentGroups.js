import React from "react";
import DataTable from "./DataTable";
//import AddData from "./crud_actions/AddData";
import AddGroup from "./crud_actions/AddGroup";

import Button from "react-bootstrap/Button";

class studentgroups extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      studentgroups: [],
      addDataPressed: false,
      isLoading: false
    };
    this.loadData = this.loadData.bind(this);
    this.handleAddGroup = this.handleAddGroup.bind(this);
    //  this.handleAddData = this.handleAddData.bind(this);
    this.handleDataChange = this.handleDataChange.bind(this);
  }

  componentDidMount() {
    this.loadData();
  }



  loadData() {

    this.setState({ isLoading: true });
    fetch("http://localhost:8080/group/all-groups")
  //  fetch("https://timeschedulerapp.herokuapp.com/group/all-groups")
      .then(response => response.json())
      .then(data =>
        this.setState({
          studentgroups: data,

          isLoading: false
        })
      );
  }

  handleAddGroup() {
    this.setState({
      addGroupPressed: !this.state.addGroupPressed
    });
  }



  handleDataChange(actionPressed, recordId) {
    if (actionPressed) {
      this.deleteGroup(recordId);
    }
  }

  render() {
    const { studentgroups, addGroupPressed, isLoading } = this.state;
    if (isLoading) {
      return <p>Is Loading...</p>;
    }
    if (this.state.addGroupPressed) {
      return <AddGroup typeOfData="group" handleAddGroup={this.addGroup} />;
    }
    return (
      <div>
        <Button variant="success" onClick={this.handleAddGroup}>
          Add Group
        </Button>
        <DataTable
          data={studentgroups}
          dataValueName="number"
          dataType="group"
          dataChanged={this.handleDataChange}
        />
      </div>
    );
  }
}

export default studentgroups;
