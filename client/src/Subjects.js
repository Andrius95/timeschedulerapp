import React from "react";
import DataTable from "./DataTable";
import AddData from "./crud_actions/AddData";
import Button from "react-bootstrap/Button";

class Subjects extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      subjects: [],
      addDataPressed: false,
      isLoading: false
    };
    this.loadData = this.loadData.bind(this);
    this.handleAddData = this.handleAddData.bind(this);
    this.addSubject = this.addSubject.bind(this);
    this.deleteSubject = this.deleteSubject.bind(this);
    this.handleDataChange = this.handleDataChange.bind(this);
  }

  componentDidMount() {
    this.loadData();
  }

  addSubject(isSubjectSaved, newSubjectName) {
    if (isSubjectSaved !== true) {
      this.handleAddData();
    } else {
      let subject = { courseName: "newSubjectName" };
      subject.courseName = newSubjectName;
      fetch("http://localhost:8080/subject/add-subject", {
        // fetch("https://timeschedulerapp.herokuapp.com/subject/add-subject", {
        body: JSON.stringify(subject),
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => this.loadData())
        .then(this.handleAddData());
    }
  }

  loadData() {
    this.setState({ isLoading: true });
    fetch("http://localhost:8080/subject/all-subjects")
//    fetch("https://timeschedulerapp.herokuapp.com/subject/all-subjects")
      .then(response => response.json())
      .then(data =>
        this.setState({
          subjects: data,
          isLoading: false
        })
      );
  }

  handleAddData() {
    this.setState({
      addDataPressed: !this.state.addDataPressed
    });
  }

  deleteSubject(recordId) {
    fetch("http://localhost:8080/subject/delete-subject/" + recordId, {
  //    fetch("https://timeschedulerapp.herokuapp.com/subject/delete-subject/" + recordId, {
      method: "DELETE"
    }).then(response => this.loadData());
  }

  handleDataChange(actionPressed, recordId) {
    if (actionPressed) {
      this.deleteSubject(recordId);
    }
  }

  render() {
    const { subjects, addDataPressed, isLoading } = this.state;
    if (isLoading) {
      return <p>Is Loading...</p>;
    }
    if (this.state.addDataPressed) {
      return <AddData typeOfData="subject" handleAddData={this.addSubject} />;
    }
    return (
      <div>
        <Button variant="success" onClick={this.handleAddData}>
          Add Subject
        </Button>
        <DataTable
          data={subjects}
          dataValueName="courseName"
          dataType="subject"
          dataChanged={this.handleDataChange}
        />
      </div>
    );
  }
}

export default Subjects;
