import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
//import AddGroup from "./AddGroup";
//import AddDataCourse from "./AddCourse";
//import AddTeacherPopup from "../AddTeacherPopup";


class AddGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      groupName: ""
    };


    // this.handleAddGroup = this.handleAddGroup.bind(this);

    this.handleChange = this.handleChange.bind(this);
    this.handleHidePopup = this.handleHidePopup.bind(this);
  }

  handleChange({ target }) {
    this.setState({
      groupName: target.value
    });
  }
  handleAddGroup({ target }) {
    this.setState({
      groupName: target.value
    });
  }
  /* saveGroup = (e) => {
          e.preventDefault();
          let user = {groupname: this.state.groupname};
          this.addGroup(group)
              .then(res => {
                  this.setState({message : 'User added successfully.'});
                  this.props.history.push('/users');
              });
      }
  */
  handleHidePopup(isGroupSaved) {
    if (isGroupSaved) {
      this.props.handleAddGroup(isGroupSaved, this.state.groupName);
    } else {
      this.props.handleAddGroup(isGroupSaved);
    }
  }

  render() {
    return (
      <Modal show={true} onHide={() => this.handleHidePopup(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Group</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Default
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.groupName}
              onChange={this.handleChange}
            />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => this.handleHidePopup(false)}
            variant="secondary"
          >
            Close
          </Button>
          <Button onClick={() => this.handleHidePopup(true)} variant="primary">
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default AddGroup;
