import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import AddDataSubject from "./AddDataSubject";
import AddDataCourse from "./AddCourse";
import AddTeacherPopup from "../AddTeacherPopup";

class AddCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var typeOfDataToAdd = this.props.typeOfData;
    var addDataInfo;
    if (typeOfDataToAdd === "course") {
      addDataInfo = <AddDataSubject handleAddData={this.props.handleAddData} />;
    }
    return addDataInfo;
  }
}

export default AddCourse;
