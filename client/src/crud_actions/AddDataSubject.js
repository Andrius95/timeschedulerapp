import React from "react";
import useState from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Dropdown from 'react-bootstrap/Dropdown';

class AddDataSubject extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      subjectName: "",
      timesPerWeek: 0,
      classroomType: "",
      teacher: {},
      groups: [{}],
      teacherList: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleHidePopup = this.handleHidePopup.bind(this);
    this.decideToggle = this.decideToggle.bind(this);
    this.setTeacher = this.setTeacher.bind(this);
  }

  componentDidMount() {
    fetch("http://localhost:8080/teacher/all-teachers")
      //  fetch("https://timeschedulerapp.herokuapp.com/teacher/all-teachers")
      .then(response => response.json())
      .then(data =>
        this.setState({
          teacherList: data,
        })
      );
  }

  handleChange({ target }) {
    this.setState({
      [target.id]: target.value
    });
  }

  handleHidePopup(isSubjectSaved) {
    if (isSubjectSaved) {
      this.props.handleAddData(isSubjectSaved, this.state.subjectName);
    } else {
      this.props.handleAddData(isSubjectSaved);
    }
  }

  decideToggle() {
    if (Object.entries(this.state.teacher).length === 0) {
      return 'Išskleisti sąrašą'
    } else {
      return this.state.teacher.name
    }
  }

  setTeacher(event) {
    let chosenTeacher = this.state.teacherList.filter(teacher => {
      if (teacher.id === parseInt(event)) {
        return teacher
      }
    })
    if (chosenTeacher.length !== 0 && chosenTeacher.length === 1) {
      this.setState({
        teacher: chosenTeacher[0]
      })
    }
    console.log(event)
  }

  render() {

    return (
      <Modal show={true} onHide={() => this.handleHidePopup(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Subject</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Dalyko pavadinimas
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="subjectName"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.subjectName}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Kartai per savaitę
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="timesPerWeek"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.timesPerWeek}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Reikiamos auditorijos tipas
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="classroomType"
              aria-label="classroomType"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.classroomType}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Pasirinkite dėstytoją
              </InputGroup.Text>
            </InputGroup.Prepend>

            <Dropdown onSelect={this.setTeacher}>
              <Dropdown.Toggle id="dropdown-custom-components">
                {this.decideToggle()}
              </Dropdown.Toggle>

              <Dropdown.Menu>
                {this.state.teacherList.map(teacher => {
                  return (
                    <Dropdown.Item eventKey={teacher.id}>{teacher.name}</Dropdown.Item>
                  );
                }
                )}
              </Dropdown.Menu>
            </Dropdown>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Pasirinkite grupes
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="groups"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.groups}
              onChange={this.handleChange}
            />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => this.handleHidePopup(false)}
            variant="secondary"
          >
            Atšaukti
          </Button>
          <Button onClick={() => this.handleHidePopup(true)} variant="primary">
            Išsaugoti
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default AddDataSubject;
