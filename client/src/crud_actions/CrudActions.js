import React from "react";
import Button from "react-bootstrap/Button";

class CrudActions extends React.Component {
  constructor(props) {
    super(props);
    this.deleteRecord = this.deleteRecord.bind(this);
  }
  deleteRecord(recordId) {
    this.props.isActionPressed(true, recordId);
  }
  render() {
    const recordId = this.props.recordId;
    return (
      <div>
        <Button variant="danger" onClick={() => this.deleteRecord(recordId)}>
          Delete
        </Button>
      </div>
    );
  }
}

export default CrudActions;
