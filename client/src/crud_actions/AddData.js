import React from "react";
import AddDataSubject from "./AddDataSubject";
import AddTeacherPopup from "../AddTeacherPopup";

class AddData extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var typeOfDataToAdd = this.props.typeOfData;
    var addDataInfo;
    if (typeOfDataToAdd === "subject") {
      addDataInfo = <AddDataSubject handleAddData={this.props.handleAddData} />;
    } else if (typeOfDataToAdd === "teacher") {
      addDataInfo = <AddTeacherPopup showPopup={this.props.handleAddData} />;
    }
    return addDataInfo;
  }
}

export default AddData;
