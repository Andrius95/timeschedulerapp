import React from "react";
import DataTable from "./DataTable";
import AddData from "./crud_actions/AddData";
import Button from "react-bootstrap/Button";

class activities extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activities: [],
      addDataPressed: false,
      isLoading: false,
    };
    this.loadData = this.loadData.bind(this);



   // this.deleteActivity = this.deleteActivity.bind(this);
    this.handleDataChange = this.handleDataChange.bind(this);
  }

  componentDidMount() {
    this.loadData();

  }


  loadData() {


    this.setState({ isLoading: true });
     fetch("http://localhost:8080/activity/group-activities")
    //   fetch("https://timeschedulerapp.herokuapp.com/activity/group-activities")
      .then(response => response.json())
      .then(data =>
        this.setState({
          activities: data,
          isLoading: false
        })
      );
  }

  handleDataChange(actionPressed, recordId) {
    if (actionPressed) {
      this.deleteActivity(recordId);
    }
  }

  render() {
    const { groups, activities, isLoading } = this.state;
    if (isLoading) {
      return <p>Is Loading...</p>;
    }
    if (this.state.addDataPressed) {
      return <AddData typeOfData="activity" handleAddData={this.addActivity} />;
    }
    return (
      <div>

        <DataTable
          data={activities}
          dataType="roomactivity"
          dataChanged={this.handleDataChange}
        />
      </div>
    );
  }
}

export default activities;
