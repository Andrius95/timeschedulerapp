import React from "react";
import MyNavbar from "./MyNavbar";
import Teachers from "./Teachers";
import StudentGroups from "./StudentGroups";
import Subjects from "./Subjects";
import Activities from "./Activities";
import Teachersactivities from "./Teachersactivities";
import Roomsactivities from "./Roomsactivities";
import Layout from "./Layout";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "Pradinis"
    };
    this.handleNextTab = this.handleNextTab.bind(this);
  }
  handleNextTab(newTab) {
    this.setState({
      activeTab: newTab
    });
  }
  render() {
    let tab;
    if (this.state.activeTab === "Teachers") {
      tab = <Teachers />;
    } else if (this.state.activeTab === "Subjects") {
      tab = <Subjects />;
    } else if (this.state.activeTab === "Activities") {
      tab = <Activities />;
    } else if (this.state.activeTab === "StudentGroups") {
      tab = <StudentGroups />;
    }
    else if (this.state.activeTab === "Teachersactivities") {
      tab = <Teachersactivities />;
    }
    else if (this.state.activeTab === "Roomsactivities") {
      tab = <Roomsactivities />;
    }
    return (
      <Layout>
        <div className="App">
          <header fixed="top" className="App-header">
            <MyNavbar activeTab={this.handleNextTab} />
          </header>
          <div>{tab}</div>
        </div>
      </Layout>
    );
  }
}

export default App;
