import React from "react";
import Table from "react-bootstrap/Table";
import CrudActions from "./crud_actions/CrudActions";

function MySubjectTable(props) {
  //r();
  const dataArray = props.dataArray;
  const groups = dataArray.groups;
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Title</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {dataArray.map(singleData => (
          <tr key={singleData.id}>
            <td>{dataArray.indexOf(singleData) + 1}</td>
            <td>{singleData.courseName}</td>
            <td>
              <CrudActions
                recordId={singleData.id}
                isActionPressed={props.isActionPressed}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}

function MyTeacherTable(props) {
  const dataArray = props.dataArray;
  const teachers = [];
  {
    dataArray.map(s => {
      teachers.push(s);

    }
    )
  }
  var k = 0;
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Nr.</th>
          <th>Vardas, pavardė</th>
          <th>Veiksmai</th>
        </tr>
      </thead>
      <tbody>
        {dataArray.map(singleData => (

          <tr key={singleData.id}>
            <td>{dataArray.indexOf(singleData) + 1}</td>
            <td>{singleData.name}</td>



            <td>
              <CrudActions
                recordId={singleData.id}
                isActionPressed={props.isActionPressed}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}
function MyGroupTable(props) {

  const dataArray = props.dataArray;
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Number</th>
          <th>Course</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {dataArray.map(singleData => (
          <tr key={singleData.id}>
            <td>{dataArray.indexOf(singleData) + 1}</td>
            <td>{singleData.number}</td>
            <td>{singleData.course.name}</td>
            <td>
              <CrudActions
                recordId={singleData.id}
                isActionPressed={props.isActionPressed}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}



//tvarkarastis pagal destytojus
//sudarytas hashmap cgrouplect1 pagal destytojus
//kiekvienoj eilutej destytojo paskaitos su laiku
function MyActivityTable1(props) {

  const dataArray = props.dataArray;
  //distinct teachers
  const teachers1 = [];
  {
    dataArray.map(s => {
      teachers1.push(s.subject.teacher);
      //console.log(s.subject.groups[0].number+',jjj'+dataArray.length)
    }
    )//.join(",")
  }
  teachers1.sort((a, b) => (a.name > b.name) ? 1 : -1)
  const teachers = [];
  const map1 = new Map();
  for (const item of teachers1) {
    if (!map1.has(item.id)) {
      map1.set(item.id, true);    // set any value to Map
      teachers.push(item);
    }
  }
  const activities = [];
  {
    dataArray.map(s => {
      activities.push(s);
      // console.log(s.time+s.subject.courseName+',jjj'+dataArray.length+s.classroom)
    }
    )
  }
  var cgrouplect1 = new Array();
  i = 0;
  for (const item of teachers) {
    cgrouplect1[i] = new Array();
    j = 0; for (const item1 of activities) {
      if (item.name == item1.subject.teacher.name) {
        ;
        cgrouplect1[i][j] = item1;
        //	 console.log(item1.classroom);
        j++;
      }

    }
    i++;
  }
  var s;
  const lectgroup2 = new Array();
  for (var i = 0; i < teachers.length; i++) {
    lectgroup2[i] = new Array();
    //lectgroup2 - sudaromas tvarkarastis kiekvienai savaites dienai - 25 paskaitoms, visiems dėstytojams
    for (var k = 0; k < 26; k++) {
      {
        for (var j = 0; j < cgrouplect1[i].length; j++) {
          if (k == cgrouplect1[i][j].time - 1 && cgrouplect1[i][j].subject.teacher.name == teachers[i].name) {//sukuriame eilutė g, jeigu dėstytojas dėsto srautui - keletui grupių
            var g = "";
            for (let v = 0; v < cgrouplect1[i][j].subject.groups.length; v++) {
              g = g + " " + cgrouplect1[i][j].subject.groups[v].number + " grupė ";
            }
            lectgroup2[i][k] = {
              teacher: cgrouplect1[i][j].subject.teacher.name, subject: cgrouplect1[i][j].subject.courseName,
              classroom: cgrouplect1[i][j].classroom.name, course: cgrouplect1[i][j].subject.groups[0].course.name, group: g
            }; //cgrouplect1[i][j];
            break;
          } else lectgroup2[i][k] = { teacher: '', subject: '', classroom: '', course: '', group: '' };;
        }
      }
    }
  }
  var cgrouplect1 = new Array();
  i = 0;
  for (const item of teachers) {
    cgrouplect1[i] = new Array();
    j = 0; for (const item1 of activities) {
      if (item.name == item1.subject.teacher.name) {
        ;
        cgrouplect1[i][j] = item1;
        // console.log(item1.classroom);
        j++;
      }

    }
    i++;
  }
  return (
    <Table striped bordered hover>
      {teachers.map(s => (
        lectgroup2.map(a => {
          if (teachers.indexOf(s) == lectgroup2.indexOf(a)) {
            return (

              <div>
                <h1>{s.name}  </h1>
                <thead>
                  <tr>
                    <th>Laikas</th>
                    <th>Pirmadienis</th>
                    <th>Antradienis</th>
                    <th>Trečiadienis</th>
                    <th>Ketvirtadienis</th>
                    <th>Penktadienis</th>
                  </tr>
                </thead>
                <tr><td>8:00-9:30</td><td> <font color="DodgerBlue">{a[0].subject}</font><br />{a[0].course} <br />{a[0].group} <br />{a[0].classroom}</td><td><font color="DodgerBlue"> {a[5].subject}</font><br />{a[5].course} <br />{a[5].group} <br />{a[5].classroom}</td><td> <font color="DodgerBlue">{a[10].subject}</font><br />{a[10].course} <br />{a[10].group} <br />{a[10].classroom}</td><td><font color="DodgerBlue">{a[15].subject}</font><br />{a[15].course}<br /> {a[15].group} <br />{a[15].classroom}</td><td><font color="DodgerBlue">{a[20].subject}</font><br />{a[20].course}<br /> {a[20].group} <br />{a[20].classroom}</td></tr>
                <tr><td>9:40-11:10</td><td> <font color="DodgerBlue">{a[1].subject}</font><br />{a[1].course}<br /> {a[1].group} <br />{a[1].classroom}</td><td> <font color="DodgerBlue">{a[6].subject}</font><br />{a[6].course}<br /> {a[6].group} <br />{a[6].classroom}</td><td> <font color="DodgerBlue">{a[11].subject}</font><br />{a[11].course}<br /> {a[11].group} <br />{a[11].classroom}</td><td><font color="DodgerBlue">{a[16].subject}</font><br />{a[16].course}<br /> {a[16].group} <br />{a[16].classroom}</td><td><font color="DodgerBlue">{a[21].subject}</font><br />{a[21].course}<br /> {a[21].group} <br />{a[21].classroom}</td></tr>
                <tr><td>11:10-12:40</td><td> <font color="DodgerBlue">{a[2].subject}</font><br />{a[2].course} <br />{a[2].group} <br />{a[2].classroom}</td><td> <font color="DodgerBlue">{a[7].subject}</font><br />{a[7].course} <br />{a[7].group} <br />{a[7].classroom}</td><td> <font color="DodgerBlue">{a[12].subject}</font><br />{a[12].course}<br /> {a[12].group} <br />{a[12].classroom}</td><td> <font color="DodgerBlue">{a[17].subject}</font><br />{a[17].course}<br /> {a[17].group} <br />{a[17].classroom}</td><td> <font color="DodgerBlue">{a[22].subject}</font><br />{a[22].course}<br /> {a[22].group} <br />{a[22].classroom}</td></tr>
                <tr><td>13:00-14:30</td><td> <font color="DodgerBlue">{a[3].subject}</font><br />{a[3].course} <br />{a[3].group} <br />{a[3].classroom}</td><td> <font color="DodgerBlue">{a[8].subject}</font><br />{a[8].course}<br />{a[8].group} <br />{a[8].classroom}</td><td><font color="DodgerBlue"> {a[13].subject}</font><br />{a[13].course} <br />{a[13].group} <br />{a[13].classroom}</td><td> <font color="DodgerBlue">{a[18].subject}</font><br />{a[18].course}<br /> {a[18].group} <br />{a[18].classroom}</td><td> <font color="DodgerBlue">{a[23].subject}</font><br />{a[23].course}<br /> {a[23].group} <br />{a[23].classroom}</td></tr>
                <tr><td>14:40-16:10</td><td> <font color="DodgerBlue">{a[4].subject}</font><br />{a[4].course}<br /> {a[4].group} <br />{a[4].classroom}</td><td> <font color="DodgerBlue">{a[9].subject}</font><br />{a[9].course} <br />{a[9].group} <br />{a[9].classroom}</td><td><font color="DodgerBlue"> {a[14].subject}</font><br />{a[14].course} <br />{a[14].group} <br />{a[14].classroom}</td><td><font color="DodgerBlue"> {a[19].subject}</font><br />{a[19].course}<br /> {a[19].group} <br />{a[19].classroom}</td><td> <font color="DodgerBlue">{a[24].subject}</font><br />{a[24].course}<br /> {a[24].group} <br />{a[24].classroom}</td></tr>

                <tr></tr>
                <tr></tr>

              </div>
            )
          };
        })
      ))
      }
    </Table>
  );

}

//tvarkarastis pagal grupes
function MyActivityTable(props) {

  const dataArray = props.dataArray;
  const activities = [];
  var k = 0;
  {
    dataArray.map(s => {
      activities.push(s);
    }
    )
  }

  const groups1 = [];
  {
    dataArray.map(s => {
      if (s.subject.groups.length > 1) {
        groups1.push(s.subject.groups[0]);
        groups1.push(s.subject.groups[1]);
      }
      else
        if (s.subject.groups.length == 1)
          groups1.push(s.subject.groups[0]);
    }
    )
  }
  groups1.sort((a, b) => (a.id > b.id) ? 1 : -1)
  const groups = [];
  const map1 = new Map();
  for (const item of groups1) {
    if (!map1.has(item.id)) {
      map1.set(item.id, true);    // set any value to Map
      groups.push(item);
    }
  }

  const subjects = [];
  {
    dataArray.map(s => {
      subjects.push(s.subject);
    }
    )
  }
  const activities1 = [];
  {
    dataArray.map(s => {
      activities1.push(s);
    }
    )
  }
  var cgrouplect1 = new Array();
  i = 0;
  for (const item of groups) {
    cgrouplect1[i] = new Array();
    j = 0; for (const item1 of activities1) {
      //jeigu tas pats dalykas dėstomas sraute reikia jį pridėti prie visų grupių, todėl reikalingas ciklas
      for (let v = 0; v < item1.subject.groups.length; v++) {
        if (item.number == item1.subject.groups[v].number && item.course.name == item1.subject.groups[v].course.name) {
          ;
          cgrouplect1[i][j] = item1;
          j++;
        }
      }
    }

    i++;
  }

  var lectgroupcourses = new Array();
  var mapgrdal2 = new Map(); var i, j; i = 0;
  for (const item of groups) {
    lectgroupcourses[i] = new Array();
    j = 0;
    for (const item1 of activities1) {

      if (item.number == item1.subject.groups[0].number && item.course.name == item1.subject.groups[0].course.name) {
        lectgroupcourses[i][j] = item1;
        j++;
      }
    }
    mapgrdal2.set(item, lectgroupcourses[i]);
    i++;
  }
  let s = '';
  const lectgroup = new Array();
  for (var i = 0; i < groups.length; i++) {
    lectgroup[i] = new Array();
    for (var k = 0; k < 26; k++) {
      {
        for (var j = 0; j < cgrouplect1[i].length; j++) {
          if (k == cgrouplect1[i][j].time - 1 && cgrouplect1[i][j].subject.groups[0].number == groups[i].number) {
            lectgroup[i][k] = s; break;
          } else lectgroup[i][k] = ' - ';
        }
      }
    }
  }
  const lectgroup2 = new Array();
  for (var i = 0; i < groups.length; i++) {
    lectgroup2[i] = new Array();
    for (var k = 0; k < 26; k++) {
      {
        for (var j = 0; j < cgrouplect1[i].length; j++) {
          if (k == cgrouplect1[i][j].time - 1 &&
            (cgrouplect1[i][j].subject.groups[0].number == groups[i].number || cgrouplect1[i][j].subject.groups[1].number == groups[i].number)) {
            lectgroup2[i][k] = { teacher: cgrouplect1[i][j].subject.teacher.name, subject: cgrouplect1[i][j].subject.courseName, classroom: cgrouplect1[i][j].classroom.name }; //cgrouplect1[i][j];

            break;
          } else lectgroup2[i][k] = { teacher: '', subject: '', classroom: '' };;
        }
      }
    }
  }

  //const listgroups = groups.map((group) =>
  // <li>{group.number}</li>
  //);

  return (

    <Table striped bordered hover>
      {groups.map(s => (
        lectgroup2.map(a => {
          if (groups.indexOf(s) == lectgroup2.indexOf(a)) {
            return (
              <div>
                <h1>{s.number} grupė {s.course.name}</h1>
                <thead>
                  <tr>
                    <th>Laikas</th>
                    <th>Pirmadienis</th>
                    <th>Antradienis</th>
                    <th>Trečiadienis</th>
                    <th>Ketvirtadienis</th>
                    <th>Penktadienis</th>
                  </tr>
                </thead>
                <tr><td>8:00-9:30</td><td> <font color="DodgerBlue">{a[0].subject}</font><br />{a[0].teacher}<br />{a[0].classroom}</td><td><font color="DodgerBlue"> {a[5].subject}</font><br />{a[5].teacher}<br />{a[5].classroom}</td><td> <font color="DodgerBlue">{a[10].subject}</font><br />{a[10].teacher}<br />{a[10].classroom}</td><td><font color="DodgerBlue">{a[15].subject}</font><br />{a[15].teacher}<br />{a[15].classroom}</td><td><font color="DodgerBlue">{a[20].subject}</font><br />{a[20].teacher}<br />{a[20].classroom}</td></tr>
                <tr><td>9:40-11:10</td><td> <font color="DodgerBlue">{a[1].subject}</font><br />{a[1].teacher}<br />{a[1].classroom}</td><td> <font color="DodgerBlue">{a[6].subject}</font><br />{a[6].teacher}<br />{a[6].classroom}</td><td> <font color="DodgerBlue">{a[11].subject}</font><br />{a[11].teacher}<br />{a[11].classroom}</td><td><font color="DodgerBlue">{a[16].subject}</font><br />{a[16].teacher}<br />{a[16].classroom}</td><td><font color="DodgerBlue">{a[21].subject}</font><br />{a[21].teacher}<br />{a[21].classroom}</td></tr>
                <tr><td>11:10-12:40</td><td> <font color="DodgerBlue">{a[2].subject}</font><br />{a[2].teacher}<br />{a[2].classroom}</td><td> <font color="DodgerBlue">{a[7].subject}</font><br />{a[7].teacher}<br />{a[7].classroom}</td><td> <font color="DodgerBlue">{a[12].subject}</font><br />{a[12].teacher}<br />{a[12].classroom}</td><td> <font color="DodgerBlue">{a[17].subject}</font><br />{a[17].teacher}<br />{a[17].classroom}</td><td> <font color="DodgerBlue">{a[22].subject}</font><br />{a[22].teacher}<br />{a[22].classroom}</td></tr>
                <tr><td>13:00-14:30</td><td> <font color="DodgerBlue">{a[3].subject}</font><br />{a[3].teacher}<br />{a[3].classroom}</td><td> <font color="DodgerBlue">{a[8].subject}</font><br />{a[8].teacher}<br />{a[8].classroom}</td><td><font color="DodgerBlue"> {a[13].subject}</font><br />{a[13].teacher}<br />{a[13].classroom}</td><td> <font color="DodgerBlue">{a[18].subject}</font><br />{a[18].teacher}<br />{a[18].classroom}</td><td> <font color="DodgerBlue">{a[23].subject}</font><br />{a[23].teacher}<br />{a[23].classroom}</td></tr>
                <tr><td>14:40-16:10</td><td> <font color="DodgerBlue">{a[4].subject}</font><br />{a[4].teacher}<br />{a[4].classroom}</td><td> <font color="DodgerBlue">{a[9].subject}</font><br />{a[9].teacher}<br />{a[9].classroom}</td><td><font color="DodgerBlue"> {a[14].subject}</font><br />{a[14].teacher}<br />{a[14].classroom}</td><td><font color="DodgerBlue"> {a[19].subject}</font><br />{a[19].teacher}<br />{a[19].classroom}</td><td> <font color="DodgerBlue">{a[24].subject}</font><br />{a[24].teacher}<br />{a[24].classroom}</td></tr>
                <tr></tr>
                <tr></tr>
              </div>
            )
          };
        })
      ))
      }
    </Table>
  );

}
function MyRoomActivityTable(props) {
  const dataArray = props.dataArray;
  //distinct rooms
  const rooms1 = [];
  {
    dataArray.map(s => {
      rooms1.push(s.classroom);
    }
    )
  }
  const rooms = [];
  const map1 = new Map();
  for (const item of rooms1) {
    if (!map1.has(item.id)) {
      map1.set(item.id, true);    // set any value to Map
      rooms.push(item);
    }
  }
  //surikiuojamos auditorijas pagal id
  rooms.sort((a, b) => (a.id > b.id) ? 1 : -1)
  const activities = [];
  {
    dataArray.map(s => {
      activities.push(s);
    }
    )
  }
  var cgrouplect1 = new Array();
  i = 0;
  for (const item of rooms) {
    cgrouplect1[i] = new Array();
    j = 0; for (const item1 of activities) {
      if (item.name == item1.classroom.name) {
        cgrouplect1[i][j] = item1;
        j++;
      }

    }
    i++;
  }
  var s;
  const lectgroup2 = new Array();
  for (var i = 0; i < rooms.length; i++) {
    lectgroup2[i] = new Array();
    //lectgroup2 - sudaromas tvarkarastis kiekvienai savaites dienai - 25 paskaitoms, visiems dėstytojams
    for (var k = 0; k < 26; k++) {
      {
        for (var j = 0; j < cgrouplect1[i].length; j++) {
          if (k == cgrouplect1[i][j].time - 1 && cgrouplect1[i][j].classroom.name == rooms[i].name) {
            //sukuriame eilutė g, jeigu auditorijoje dėstomas srautas - keletui grupių
            var g = "";
            for (let v = 0; v < cgrouplect1[i][j].subject.groups.length; v++) {
              g = g + " " + cgrouplect1[i][j].subject.groups[v].number + " grupė ";
            }
            lectgroup2[i][k] = {
              teacher: cgrouplect1[i][j].subject.teacher.name, subject: cgrouplect1[i][j].subject.courseName,
              classroom: cgrouplect1[i][j].classroom.name, course: cgrouplect1[i][j].subject.groups[0].course.name, group: g
            }; //cgrouplect1[i][j];
            break;
          } else lectgroup2[i][k] = { teacher: '', subject: '', classroom: '', course: '', group: '' };;
        }
      }
    }
  }
  var cgrouplect1 = new Array();
  i = 0;
  for (const item of rooms) {
    cgrouplect1[i] = new Array();
    j = 0; for (const item1 of activities) {
      if (item.name == item1.classroom.name) {
        ;
        cgrouplect1[i][j] = item1;
        // console.log(item1.classroom);
        j++;
      }

    }
    i++;
  }
  return (
    <Table striped bordered hover>
      {rooms.map(s => (
        lectgroup2.map(a => {
          if (rooms.indexOf(s) == lectgroup2.indexOf(a)) {
            return (

              <div>
                <h1>{s.name}  {s.type}  </h1>
                <thead>
                  <tr>
                    <th>Laikas</th>
                    <th>Pirmadienis</th>
                    <th>Antradienis</th>
                    <th>Trečiadienis</th>
                    <th>Ketvirtadienis</th>
                    <th>Penktadienis</th>
                  </tr>
                </thead>
                <tr><td>8:00-9:30</td><td> <font color="DodgerBlue">{a[0].subject}</font><br />{a[0].course} <br />{a[0].group} <br />{a[0].teacher}</td><td><font color="DodgerBlue"> {a[5].subject}</font><br />{a[5].course} <br />{a[5].group} <br />{a[5].teacher}</td><td> <font color="DodgerBlue">{a[10].subject}</font><br />{a[10].course} <br />{a[10].group} <br />{a[10].teacher}</td><td><font color="DodgerBlue">{a[15].subject}</font><br />{a[15].course}<br /> {a[15].group} <br />{a[15].teacher}</td><td><font color="DodgerBlue">{a[20].subject}</font><br />{a[20].course}<br /> {a[20].group} <br />{a[20].teacher}</td></tr>
                <tr><td>9:40-11:10</td><td> <font color="DodgerBlue">{a[1].subject}</font><br />{a[1].course}<br /> {a[1].group} <br />{a[1].teacher}</td><td> <font color="DodgerBlue">{a[6].subject}</font><br />{a[6].course}<br /> {a[6].group} <br />{a[6].teacher}</td><td> <font color="DodgerBlue">{a[11].subject}</font><br />{a[11].course}<br /> {a[11].group} <br />{a[11].teacher}</td><td><font color="DodgerBlue">{a[16].subject}</font><br />{a[16].course}<br /> {a[16].group} <br />{a[16].teacher}</td><td><font color="DodgerBlue">{a[21].subject}</font><br />{a[21].course}<br /> {a[21].group} <br />{a[21].teacher}</td></tr>
                <tr><td>11:10-12:40</td><td> <font color="DodgerBlue">{a[2].subject}</font><br />{a[2].course} <br />{a[2].group} <br />{a[2].teacher}</td><td> <font color="DodgerBlue">{a[7].subject}</font><br />{a[7].course} <br />{a[7].group} <br />{a[7].teacher}</td><td> <font color="DodgerBlue">{a[12].subject}</font><br />{a[12].course}<br /> {a[12].group} <br />{a[12].teacher}</td><td> <font color="DodgerBlue">{a[17].subject}</font><br />{a[17].course}<br /> {a[17].group} <br />{a[17].teacher}</td><td> <font color="DodgerBlue">{a[22].subject}</font><br />{a[22].course}<br /> {a[22].group} <br />{a[22].teacher}</td></tr>
                <tr><td>13:00-14:30</td><td> <font color="DodgerBlue">{a[3].subject}</font><br />{a[3].course} <br />{a[3].group} <br />{a[3].teacher}</td><td> <font color="DodgerBlue">{a[8].subject}</font><br />{a[8].course}<br />{a[8].group} <br />{a[8].teacher}</td><td><font color="DodgerBlue"> {a[13].subject}</font><br />{a[13].course} <br />{a[13].group} <br />{a[13].teacher}</td><td> <font color="DodgerBlue">{a[18].subject}</font><br />{a[18].course}<br /> {a[18].group} <br />{a[18].teacher}</td><td> <font color="DodgerBlue">{a[23].subject}</font><br />{a[23].course}<br /> {a[23].group} <br />{a[23].teacher}</td></tr>
                <tr><td>14:40-16:10</td><td> <font color="DodgerBlue">{a[4].subject}</font><br />{a[4].course}<br /> {a[4].group} <br />{a[4].teacher}</td><td> <font color="DodgerBlue">{a[9].subject}</font><br />{a[9].course} <br />{a[9].group} <br />{a[9].teacher}</td><td><font color="DodgerBlue"> {a[14].subject}</font><br />{a[14].course} <br />{a[14].group} <br />{a[14].teacher}</td><td><font color="DodgerBlue"> {a[19].subject}</font><br />{a[19].course}<br /> {a[19].group} <br />{a[19].teacher}</td><td> <font color="DodgerBlue">{a[24].subject}</font><br />{a[24].course}<br /> {a[24].group} <br />{a[24].teacher}</td></tr>

                <tr></tr>
                <tr></tr>

              </div>
            )
          };
        })
      ))
      }
    </Table>
  );

}


class DataTable extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data
    };
    this.isCrudActionPressed = this.isCrudActionPressed.bind(this);
  }
  isCrudActionPressed(isActionPressed, recordId) {
    this.props.dataChanged(isActionPressed, recordId);
  }

  render() {


    const dataArray = this.state.data;
    //  const groups = this.state.data1;
    var dataType = this.props.dataType;
    var table;
    //  for (var i = 1; i < 3; i++) {
    if (dataType === "subject") {
      table = (
        <MySubjectTable
          dataArray={dataArray}
          isActionPressed={this.isCrudActionPressed}
        />
      );
    } else if (dataType === "teacher") {
      table = (
        <MyTeacherTable
          dataArray={dataArray}
          isActionPressed={this.isCrudActionPressed}
        />
      );
    } else if (

      dataType === "activity") {
      table = (
        <MyActivityTable
          dataArray={dataArray}

          isActionPressed={this.isCrudActionPressed}

        />
      );
    }
    else if (

      dataType === "roomactivity") {
      table = (
        <MyRoomActivityTable
          dataArray={dataArray}
          isActionPressed={this.isCrudActionPressed}

        />
      );
    }
    else if (dataType === "group") {
      table = (
        <MyGroupTable
          dataArray={dataArray}
          //	  groups={groups}
          isActionPressed={this.isCrudActionPressed}
        />
      );
    }
    else if (dataType === "teachersactivity") {

      table = (
        <MyActivityTable1
          dataArray={dataArray}
          isActionPressed={this.isCrudActionPressed}
        />
      );
    }
    return table;
    //  }
  }
}

export default DataTable;
