import React from "react";
import DataTable from "./DataTable";
import AddData from "./crud_actions/AddData";
import Button from "react-bootstrap/Button";
import Dropdown from "react-bootstrap/Dropdown";

class activities extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activities: [],
      addDataPressed: false,
      isLoading: false,
      scheduleType: 'sortedGreedy'
      //  groups: []
    };
    this.loadData = this.loadData.bind(this);

    // this.handleAddData = this.handleAddData.bind(this);
    this.handleDataChange = this.handleDataChange.bind(this);
    this.updateType = this.updateType.bind(this);
  }

  componentDidMount() {
    //this.loadData();
    this.loadData();

  }

  updateType(event) {
    this.setState({
      scheduleType: event
    }, () => {this.loadData()});
  }


  loadData() {
    var requestDataType=this.state.scheduleType
    if(!requestDataType) {
      requestDataType = 'sortedGreedy';
    }

    this.setState({ isLoading: true });
    fetch("http://localhost:8080/timetable/calculate-timetable/" + requestDataType, {
      method: "GET"
    })
    // fetch("https://timeschedulerapp.herokuapp.com/timetable/calculate-timetable/" + requestDataType, {
    //   method: "GET"
    // })
      .then(response => response.json())
      .then(data =>
        this.setState({
          activities: data,
          isLoading: false
        })
      );
  }

  handleDataChange(actionPressed, recordId) {
    if (actionPressed) {
      this.deleteActivity(recordId);
    }
  }

  render() {
    const { groups, activities, isLoading } = this.state;
    if (isLoading) {
      return <p>Is Loading...</p>;
    }
    if (this.state.addDataPressed) {
      return <AddData typeOfData="activity" handleAddData={this.addActivity} />;
    }
    return (
      <div>

      <Dropdown onSelect={this.updateType}>
  <Dropdown.Toggle variant="success" id="dropdown-basic">
    {this.state.scheduleType}
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item eventKey='sortedGreedy'>surikiuotas Godumo</Dropdown.Item>
    <Dropdown.Item eventKey='sortedBackTracking'>surikiuotas Grįžimo</Dropdown.Item>
    <Dropdown.Item eventKey='shuffleGreedy'>atsitiktinis Godumo</Dropdown.Item>
    <Dropdown.Item eventKey='shuffleBackTracking'>atsitiktinis Grįžimo</Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>

        <DataTable
          data={activities}
          //  data1={groups}
          //  dataValueName="courseName"
          dataType="activity"
          dataChanged={this.handleDataChange}
        />
      </div>
    );
  }
}

export default activities;
