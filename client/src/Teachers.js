import React from "react";
import DataTable from "./DataTable";
import AddTeacherPopup from "./AddTeacherPopup";
import Button from "react-bootstrap/Button";
import AddData from "./crud_actions/AddData";

class Teachers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      teachers: [],
      //  teacherSubjects: [],
      isLoading: false,
      showAddTeacherPopup: false
    };




    this.handleDataChange = this.handleDataChange.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.deleteTeacher = this.deleteTeacher.bind(this);
    this.displayAddTeacherPopup = this.displayAddTeacherPopup.bind(this);
    //this.teacherSubjects = this.teacherSubjects.bind(this);
  }

  handleDataChange(actionPressed, recordId) {
    if (actionPressed) {
      this.deleteTeacher(recordId);
    }
  }

  componentDidMount() {
    this.refreshData();
    // this.teacherSubjects();
  }

  refreshData() {
    this.setState({ isLoading: true });
     fetch("http://localhost:8080/teacher/all-teachers")
  //  fetch("https://timeschedulerapp.herokuapp.com/teacher/all-teachers")
      .then(response => response.json())
      .then(data =>
        this.setState({
          teachers: data,
          isLoading: false
        })
      );
  }



  deleteTeacher(recordId) {
    fetch("http://localhost:8080/teacher/delete-teacher/" + recordId, {
   // fetch("https://timeschedulerapp.herokuapp.com/teacher/delete-teacher/" + recordId, {
      method: "DELETE"
    }).then(response => this.refreshData());
  }

  displayAddTeacherPopup(teacherWasAdded, newTeacher) {
    this.setState({ isLoading: true });
    if (teacherWasAdded !== true) {
      this.setState({
        showAddTeacherPopup: !this.state.showAddTeacherPopup,
        isLoading: false
      });
    } else {
     fetch("http://localhost:8080/teacher/add-teacher", {
  //    fetch("https://timeschedulerapp.herokuapp.com/teacher/add-teacher", {
        body: JSON.stringify(newTeacher),
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => this.refreshData())
        .then(
          this.setState({
            showAddTeacherPopup: !this.state.showAddTeacherPopup,
            isLoading: false
          })
        );
    }
  }

  render() {
    const { teachers, isLoading, showAddTeacherPopup } = this.state;
    if (isLoading) {
      return <p>Is Loading...</p>;
    }
    if (showAddTeacherPopup) {
      return (
        <AddData
          typeOfData="teacher"
          handleAddData={this.displayAddTeacherPopup}
        />
      );
    }
    return (
      <div>
        <Button variant="success" onClick={this.displayAddTeacherPopup}>
          Add Teacher
        </Button>
        <DataTable
          data={teachers}
          //  data1={teacherSubjects}
          dataType="teacher"
          dataChanged={this.handleDataChange}
        />
      </div>
    );
  }
}
export default Teachers;
