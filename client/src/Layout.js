import React from "react";
import Container from 'react-bootstrap/Container'
import Image from 'react-bootstrap/Image'
import PictureSource from "./assets/logo.png"

export default function Layout({ children }) {
    return (
        <Container>
            <Image src={PictureSource} height="200px" width="100%"/>
            {children}
        </Container>
    )
}