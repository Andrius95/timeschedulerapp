import React from "react";
import DataTable from "./DataTable";
import AddTeacherPopup from "./AddTeacherPopup";
import Button from "react-bootstrap/Button";
//import AddData from "./crud_actions/AddData";

class Teachersactivities extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      teachersactivities: [],
      //  teacherSubjects: [],
      isLoading: false,
 //     showAddTeacherPopup: false
    };




  //  this.handleDataChange = this.handleDataChange.bind(this);
    this.refreshData = this.refreshData.bind(this);
  //  this.deleteTeacher = this.deleteTeacher.bind(this);
 //   this.displayAddTeacherPopup = this.displayAddTeacherPopup.bind(this);
    //this.teacherSubjects = this.teacherSubjects.bind(this);
  }



  componentDidMount() {
    this.refreshData();
    // this.teacherSubjects();
  }

  refreshData() {
    this.setState({ isLoading: true });
   fetch("http://localhost:8080/activity/group-activities")
//    fetch("https://timeschedulerapp.herokuapp.com/activity/group-activities")
      .then(response => response.json())
      .then(data =>
        this.setState({
          teachersactivities: data,
          isLoading: false
        })
      );
  }


  render() {
    const { teachersactivities, isLoading } = this.state;
    if (isLoading) {
      return <p>Is Loading...</p>;
    }
   // if (showAddTeacherPopup) {
   //   return (
    //    <AddData
      //    typeOfData="teachersactivity"
        // handleAddData={this.displayAddTeacherPopup}
      //  />
     // );
  //  }
    return (
      <div>
        <DataTable
          data={teachersactivities}
          //  data1={teacherSubjects}
          dataType="teachersactivity"
        //  dataChanged={this.handleDataChange}
        />
      </div>
    );
  }
}
export default Teachersactivities;
