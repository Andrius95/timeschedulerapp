import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";

class AddTeacherPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      teacherName: "",
      teacherLastName: "",
      title: "",
      dateOfBirth: "",
      country: "",
      city: "",
      addressLine1: "",
      addressLine2: "",
      postalCode: "",
    };
    this.handleHidePopup = this.handleHidePopup.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleHidePopup(teacherSaved) {
    var teacherAddressInfo = {};

    teacherAddressInfo.country = this.state.country;
    teacherAddressInfo.city = this.state.city;
    teacherAddressInfo.addressLine1 = this.state.addressLine1;
    teacherAddressInfo.addressLine2 = this.state.addressLine2;
    teacherAddressInfo.postalCode = this.state.postalCode;

    var teacherInfo = {}
    teacherInfo.firstName = this.state.teacherName;
    teacherInfo.lastName = this.state.teacherLastName;
    teacherInfo.title = this.state.title;
    teacherInfo.dateOfBirth = new Date();
    teacherInfo.address = teacherAddressInfo;

    let teacher = { subjects: [], times: [], teacherInfo: teacherInfo };
    teacher.name = this.state.title + " " + this.state.teacherName + " " + this.state.teacherLastName;
    if (teacherSaved) {
      this.props.showPopup(teacherSaved, teacher);
    } else {
      this.props.showPopup(teacherSaved);
    }
  }

  handleChange({ target }) {
    this.setState({
      [target.id]: target.value
    });
  }

  render() {

    let teacher = {};

    return (
      <Modal show={true} onHide={() => this.handleHidePopup(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Pridėti dėstytoją</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Vardas
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="teacherName"
              aria-label="teacherName"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.teacherName}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Pavardė
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="teacherLastName"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.teacherLastName}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Pareigos
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="title"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.title}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Gimimo Data
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="dateOfBirth"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.dateOfBirth}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Šalis
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="country"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.country}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Miestas
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="city"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.city}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Adresas 1
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="addressLine1"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.addressLine1}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Adresas 2
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="addressLine2"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.addressLine2}
              onChange={this.handleChange}
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-default">
                Pašto kodas
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              id="postalCode"
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              value={this.state.postalCode}
              onChange={this.handleChange}
            />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => this.handleHidePopup(false)}
            variant="secondary"
          >
            Atšaukti
          </Button>
          <Button onClick={() => this.handleHidePopup(true)} variant="primary">
            Saugoti
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default AddTeacherPopup;
