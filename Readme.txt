Norint pasileisti front-end programos dal� (veikia tik and chrome nar�ykl�s):

cd ~\client

pirm� kart� leid�iant: 
npm install

paleisti aplikacij�:
npm start


Norint pasileisti back-end programos dal�:

1 b�das:

cd ~\server

pirm� kart� leid�iant:
mvn install

paleisti aplikacij�:
java -jar target/timeScheduler-0.0.1-SNAPSHOT.jar

2 b�das:
Intellij IDE, susiimportuoti maven projekt�, ir TimeSchedulerApplication startuoti program�.

